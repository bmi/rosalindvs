﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace RosalindVS
{
    internal class InferringMRNAFromProtein
    {
        public InferringMRNAFromProtein()
        {
            var input = "MA";
            var table = getCodonTable();
            BigInteger result = 3;
            input
                .OfType<char>()
                .ToList()
                .ForEach(c => result *= table[c]);
            result = result % 1000000;
        }

        private Dictionary<char, int> getCodonTable()
        {
            var source = @"UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G";
            var elms = source
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();
            var chunks2 = Enumerable.Range(0, elms.Length / 2)
                .Select(idx => new string[] { elms[idx * 2], elms[idx * 2 + 1]})
                .Where(t => t[1] != "Stop")
                .ToArray();
            var dct = chunks2
                .GroupBy(g => g[1])
                .ToDictionary(k => k.Key[0], v => v.Count());
            return dct;
        }
    }
}