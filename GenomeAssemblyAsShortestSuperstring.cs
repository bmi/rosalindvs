﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class GenomeAssemblyAsShortestSuperstring
    {
        public GenomeAssemblyAsShortestSuperstring()
        {
            // Load FASTA Data from file
            var input = LoadFastaInput(@"h:\Downloads\rosalind_long.txt").ToList();

            // Create collection
            var items = new List<Item>();
            foreach (var item in input)
            {
                var itm = new Item();
                items.Add(itm);
                itm.Source = item.Value;
                itm.MinLength = ((item.Value.Length - item.Value.Length % 2) / 2) + 1;
            }

            // Find congruent postfixes and prefixes, and chain them
            foreach (var iPostfix in items)
            {
                foreach (var iPrefix in items)
                {
                    if (iPostfix.Equals(iPrefix)) continue;
                    Find(iPostfix, iPrefix);
                }
            }

            // get first string (does not have Prev node)
            var itmx = items.FirstOrDefault(i => i.Prev == null);
            var sbResult = new StringBuilder();
            sbResult.Append(itmx.Source);
            itmx = itmx.Next;
            // iterate through chained items
            while (itmx != null)
            {
                sbResult.Append(itmx.Source.Substring(itmx.Prev.ValidPostfix.Length));
                itmx = itmx.Next;
            }
            // here is result
            var str = sbResult.ToString();
        }

        /// <summary>
        /// Match strings
        /// </summary>
        /// <param name="iPostfix"></param>
        /// <param name="iPrefix"></param>
        private static void Find(Item iPostfix, Item iPrefix)
        {
            var len = Math.Min(iPrefix.MinLength, iPostfix.MinLength);
            var sPostfix = iPostfix.Source.Substring(iPostfix.Source.Length - len);
            var posInPrefix = iPrefix.Source.IndexOf(sPostfix);
            if (posInPrefix > -1)
            {
                if (posInPrefix > 0)
                {
                    // test if really continue
                    sPostfix = iPostfix.Source.Substring(iPostfix.Source.Length - len - posInPrefix);
                    if (!iPrefix.Source.StartsWith(sPostfix))
                    {
                        // does not match
                        return;
                    }
                }
                iPostfix.ValidPostfix = sPostfix;
                iPostfix.Next = iPrefix;
                iPrefix.Prev = iPostfix;
            }
        }

        /// <summary>
        /// Load fasta data
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }



    }

    [DebuggerDisplay("Src: {Source}, Next: {Next?.Source}, Prev: {Prev?.Source}, PosShift: {PosShift}")]
    class Item
    {
        public string Source { get; set; }
        public Item Prev { get; set; }
        public Item Next { get; set; }
        public int MinLength { get; internal set; }
        public string ValidPostfix { get; internal set; }
    }

}