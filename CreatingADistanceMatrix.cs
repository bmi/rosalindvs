﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class CreatingADistanceMatrix
    {
        public CreatingADistanceMatrix()
        {

            var fasta = LoadFastaInput(@"rosalind_pdst.txt");
            var inputs = fasta.Select(f => f.Value).ToArray();
            var map = new double[inputs.Length, inputs.Length];
            var strLen = inputs[0].Length;

            // get p-distance into array
            for (int i = 0; i < inputs.Length; i++)
            {
                for (int j = i + 1; j < inputs.Length; j++)
                {
                    var stri = inputs[i];
                    var strj = inputs[j];
                    var diffs = 0;
                    for (int k = 0; k < strLen; k++)
                        if (stri[k] != strj[k]) diffs++;
                    var pDistance = Math.Round((double)diffs / (double)strLen, 5);
                    map[i, j] = map[j, i] = pDistance;
                }
            }

            // get table result
            var xResult = new StringBuilder();
            for (int i = 0; i < inputs.Length; i++)
            {
                for (int j = 0; j < inputs.Length; j++)
                {
                    xResult.Append($"{map[i, j]:0.00000} ");
                }
                xResult.AppendLine();
            }
            var result = xResult.ToString(); // result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }
    }
}