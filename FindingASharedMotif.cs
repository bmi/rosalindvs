﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RosalindVS
{
    internal class FindingASharedMotif
    {
        public FindingASharedMotif()
        {
            var input = @">Rosalind_1
GATTACA
>Rosalind_2
TAGACCA
>Rosalind_3
ATACA";

            var data = getFASTAData(input);
            var minLen = data.Min(r => r.Item2.Length);                                 // optimization - search based on shortest string
            var shortestLine = data.First(r => r.Item2.Length == minLen);
            var searchedLines = data.Where(r => !r.Item2.Equals(shortestLine.Item2));   // optimization - remove all occurences of shortest line
            var result = "";
            var sw = new Stopwatch();
            sw.Start();
            for (int len = minLen; len >= 1; len--)                                     // optimization - find from longest substring
                for (int partStart = 0; partStart <= minLen - len; partStart++)
                {
                    result = shortestLine.Item2.Substring(partStart, len);
                    var found = searchedLines.All(r => r.Item2.Contains(result));
                    if (found)
                    {
                        sw.Stop();
                        var duration = sw.ElapsedMilliseconds;
                        break; // result in result variable
                    }
                }


            IEnumerable<Tuple<string, string>> getFASTAData(string strFASTA)
            {
                return strFASTA
                    .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                    .Select(strArr => new Tuple<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
            }
        }
    }
}