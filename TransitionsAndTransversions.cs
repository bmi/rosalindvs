﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RosalindVS
{
    internal class TransitionsAndTransversions
    {
        public TransitionsAndTransversions()
        {
            var data = LoadFastaInput(@"h:\Downloads\rosalind_tran.txt");
            var l1 = data.First().Value;
            var l2 = data.Last().Value;

            var t1 = 0;
            var t2 = 0;

            for (int i = 0; i < l1.Length; i++)
            {
                if (
                    l1[i] == 'A' && l2[i] == 'G'
                 || l1[i] == 'C' && l2[i] == 'T'
                 || l1[i] == 'G' && l2[i] == 'A'
                 || l1[i] == 'T' && l2[i] == 'C'
                 )
                    t1++;
                else if (l1[i] != l2[i])
                    t2++;
            }
            var result = System.Math.Round( (double)t1 / (double)t2, 11); // result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

    }
}