﻿using System;
using System.Linq;

namespace RosalindVS
{
    internal class CalculateExpectedOffspring
    {
        public CalculateExpectedOffspring()
        {
            var input = "18483 19908 16276 16607 19504 18627".Split(' ').Select(s => int.Parse(s)).ToArray();
            var probs = new double[] { 1, 1, 1, 0.75, 0.5, 0 };
            double childs = 2;
            var result = Enumerable.Range(0, 6).Sum(idx => input[idx] * probs[idx] * childs);
        }
    }
}