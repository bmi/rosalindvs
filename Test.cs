﻿using System.Numerics;
using System.Text;

public static class PermutationsAndCombinations
{

    public static void Test()
    {

        int n = 985;
        BigInteger outp = 1; // for 0 combination

        // sum combinations count of all sizes
        for (int r = 1; r <= n; r++)
            outp += nCr(n, r);

        var result = outp % 1000000; // result is here


    }

    public static BigInteger nCr(int n, int r)
    {
        // naive: return Factorial(n) / (Factorial(r) * Factorial(n - r));
        return nPr(n, r) / Factorial(r);
    }

    public static BigInteger nPr(int n, int r)
    {
        // naive: return Factorial(n) / Factorial(n - r);
        return FactorialDivision(n, n - r);
    }

    private static BigInteger FactorialDivision(int topFactorial, int divisorFactorial)
    {
        BigInteger result = 1;
        for (int i = topFactorial; i > divisorFactorial; i--)
            result *= i;
        return result;
    }

    private static BigInteger Factorial(int i)
    {
        if (i <= 1)
            return 1;
        return i * Factorial(i - 1);
    }
}