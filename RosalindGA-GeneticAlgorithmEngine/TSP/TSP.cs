﻿using GeneticAlgorithm;
using GeneticAlgorithm.Components.Chromosomes;
using GeneticAlgorithm.Components.CrossoverManagers;
using GeneticAlgorithm.Components.Interfaces;
using GeneticAlgorithm.Components.MutationManagers;
using GeneticAlgorithm.Components.PopulationGenerators;
using GeneticAlgorithm.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosalindGA_GeneticAlgorithmEngine
{
    public class TSP
    {
        private const int POPULATION_SIZE = 100;
        private const int GENERATIONS = 10000;
        private static string[] cities = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };
        private static Dictionary<string, Tuple<int, int>> locations = new Dictionary<string, Tuple<int, int>>
        {
            {"a", new Tuple<int, int>(10, 10)},
            {"b", new Tuple<int, int>(10, 200)},
            {"c", new Tuple<int, int>(1000, 10)},
            {"d", new Tuple<int, int>(500, 476)},
            {"e", new Tuple<int, int>(200, 800)},
            {"f", new Tuple<int, int>(80, 199)},
            {"g", new Tuple<int, int>(455, 78)},
            {"h", new Tuple<int, int>(511, 907)},
            {"i", new Tuple<int, int>(230, 400)},
            {"j", new Tuple<int, int>(611, 801)},
        };
        private static DistanceCalclator distanceCalclator = new DistanceCalclator(locations);

        public void Main()
        {
            Console.WriteLine("Started!");

            IMutationManager<string> mutationManager = new ExchangeMutationManager<string>();
            IEvaluator evaluator = new DistanceEvaluator(locations);
            ICrossoverManager crossoverManager = new OrderCrossover<string>(mutationManager, evaluator);
            IPopulationGenerator populationGenerator =
                new AllElementsVectorChromosomePopulationGenerator<string>(cities, mutationManager, evaluator);

            GeneticSearchEngine engine =
                new GeneticSearchEngineBuilder(POPULATION_SIZE, GENERATIONS, crossoverManager, populationGenerator)
                    .SetMutationProbability(0.8).SetElitePercentage(0.5).Build();
            engine.OnNewGeneration += (Population p, IEnvironment e) => PrintBestChromosome(p);

            GeneticSearchResult result = engine.Run();

            Console.WriteLine("Finished!");
            Console.WriteLine(result.BestChromosome + ": " + distanceCalclator.GetDistance(result.BestChromosome));

            Console.ReadLine();
        }

        private static void PrintBestChromosome(Population population)
        {
            var bestEvaluation = 0.0;
            ChromosomeEvaluationPair bestPair = null;
            foreach (ChromosomeEvaluationPair chromosomeEvaluationPair in population)
                if (chromosomeEvaluationPair.Evaluation > bestEvaluation)
                {
                    bestEvaluation = chromosomeEvaluationPair.Evaluation;
                    bestPair = chromosomeEvaluationPair;
                }

            Console.WriteLine(bestPair.Chromosome + ": " + distanceCalclator.GetDistance(bestPair.Chromosome));
        }
    }

    class DistanceEvaluator : IEvaluator
    {
        private readonly DistanceCalclator distanceCalclator;
        private readonly double maxDistance;

        public DistanceEvaluator(IDictionary<string, Tuple<int, int>> locations)
        {
            distanceCalclator = new DistanceCalclator(locations);
            maxDistance = 1500 * locations.Count;
        }

        // Note that a shorter route should give a better evaluation
        public double Evaluate(IChromosome chromosome) =>
            maxDistance - distanceCalclator.GetDistance(chromosome);
    }

    class DistanceCalclator
    {
        private readonly IDictionary<string, Tuple<int, int>> locations;

        public DistanceCalclator(IDictionary<string, Tuple<int, int>> locations)
        {
            this.locations = locations;
        }

        public double GetDistance(IChromosome chromosome)
        {
            var totalDistance = 0.0;
            var cityVector = ((VectorChromosome<string>)chromosome).GetVector();
            for (int i = 0; i < cityVector.Length - 1; i++)
                totalDistance += GetDistance(cityVector[i], cityVector[i + 1]);

            return totalDistance;
        }

        private double GetDistance(string city1, string city2) =>
            Math.Sqrt(Math.Pow(locations[city1].Item1 - locations[city2].Item1, 2) + Math.Pow(locations[city1].Item2 - locations[city2].Item2, 2));
    }
}
