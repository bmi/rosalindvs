﻿using System;
using System.Linq;

namespace RosalindVS
{
    internal class IdentifyingUnknownDNAQuickly
    {
        public IdentifyingUnknownDNAQuickly()
        {
            var source = @">Rosalind_3110
AAAAGATTAATAGCGGTGACTCGTAAACGTAGATTGGGTGCATCGTATGACGGTGTTAAA
>Rosalind_7077
TAACACATCACCGTCTCGGTCCCGCGAATACGGGTTTCCCGTTAGGCGCTCTGGGAAACC
CGTTAGGACCCCTACGC";

            var result = source
            .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
            .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
            .ToDictionary(parts => parts[0].Trim(), parts => calculateDCContent(string.Join("", parts.Skip(1))))
            .OrderByDescending(g => g.Value)
            .First();

            // this is result
            var strResult = $"{result.Key}{Environment.NewLine}{result.Value}";

            double calculateDCContent(string str)
            {
                double countCG = str.Count(c => c == 'C' || c == 'G');
                return Math.Round(countCG / str.Length * 100.0, 6);
            }
        }

    }
}