﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Randomizations;
using GeneticSharp.Domain.Reinsertions;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Policy;
using System.Threading.Tasks;

namespace RosalindVS
{
    /// <summary>
    /// Reversal Distance genetic algorithm - non deterministic solution
    /// </summary>
    internal class ReversalDistance
    {
        public const int PopMin = 100;
        public const int PopMax = 300;
        public const int Stagnation = 100;
        public const int MaxReversals = 10;
        public const int NoSolutionPenalty = 15;
        public const int StopAfter = 100;
        public const float MutationProbability = 0.1f; // original 0.1f
        public const float CrossOverMicProbability = 0.5f; // original 0.5f

        public ReversalDistance()
        {
            Console.WriteLine($"{DateTime.Now}, Best 6 7 6 6 9");
            var input = @"9 8 4 1 2 3 6 5 7 10
8 3 10 2 9 5 4 6 7 1

6 7 8 2 3 10 4 9 5 1
8 4 5 10 6 1 7 2 3 9

9 1 8 6 10 4 7 3 5 2
2 1 5 6 4 10 7 8 3 9

9 2 10 4 7 3 1 6 5 8
9 3 10 4 6 2 8 7 5 1

6 10 8 9 4 3 7 5 2 1
8 6 4 10 7 9 2 3 1 5";

            var parts = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            var inputs = Enumerable.Range(0, parts.Length >> 1)
                .Select(idx => new string[] { parts[idx * 2], parts[idx * 2 + 1] })
                .ToArray();

            var lst = new List<Asd>();
            var start = DateTime.Now;
            for (int i = 0; i < inputs.Length; i++)
            {
                var inp = inputs[i];
                for (int j = 0; j < 1; j++)
                {
                    var asd = new Asd { Str1 = inp[0], Str2 = inp[1], InputID = i + 1, Start = start };
                    lst.Add(asd);
                }
            }

            Console.WriteLine($"Population: {ReversalDistance.PopMin} - {ReversalDistance.PopMax} ");
            Console.WriteLine($"CrossOverMicProbability: {ReversalDistance.CrossOverMicProbability}");
            Console.WriteLine($"MutationProbability: {ReversalDistance.MutationProbability}");

            var pa = new ParallelOptions { MaxDegreeOfParallelism = lst.Count };
            // search for solutions - continuously on screen - stop ctrl + break or wait long, long
            Parallel.ForEach(lst, pa, (src) => GetSolutions(src));
            Console.WriteLine("Key");
            Console.ReadKey();
        }

        private int GetSolutions(Asd asd)
        {
            // same string
            if (asd.Str1 == asd.Str2)
            {
                Console.WriteLine($"{asd.InputID}: 1-{0}    ");
                return 0;
            }

            // prepare data
            var arr0 = asd.Str1.Split(' ').Select(s => int.Parse(s)).ToArray();
            var arr1 = asd.Str2.Split(' ').Select(s => int.Parse(s)).ToArray();
            var dct = new Dictionary<int, int>();
            for (int i = 0; i < arr0.Length; i++)
                dct.Add(arr0[i], i);

            // displaying searched solutions on Console
            int best = int.MaxValue;
            int idx = 0;
            while (true)
            {
                var s = GetSolution(arr0, arr1, dct);
                if (s == int.MaxValue) continue;
                if (s < best)
                {
                    best = s;
                    Console.WriteLine($"{asd.InputID}: [{best}] {Math.Round(DateTime.Now.Subtract(asd.Start).TotalMinutes, 2)}");
                }
                idx++;
                if (idx >= ReversalDistance.StopAfter)
                {
                    break;
                }
            }
            return best;
        }

        private int GetSolution(int[] arr0, int[] arr1, Dictionary<int, int> dct)
        {
            // GA defintion, used free GeneticSharp from NuGet
            var ichromosome = new ReversalDistanceChromosome(ReversalDistance.MaxReversals);
            var population = new Population(PopMin, PopMax, ichromosome);

            var fitness = new ReversalDistanceFitness(arr0, arr1, dct);
            var selection = new EliteSelection();
            var crossover = new UniformCrossover();
            crossover.MixProbability = ReversalDistance.CrossOverMicProbability;
            var mutation = new UniformMutation();
            var reinsertion = new ElitistReinsertion();
            var termination = new FitnessStagnationTermination(Stagnation);

            var ga = new GeneticAlgorithm(
                population,
                fitness,
                selection,
                crossover,
                mutation);
            ga.Reinsertion = reinsertion;

            ga.Termination = termination;
            ga.MutationProbability = ReversalDistance.MutationProbability;

            var latestFitness = 0.0;
            var bestSolution = int.MaxValue;

            ga.GenerationRan += (sender, e) =>
            {
                // get best solutions
                var chr = ga.BestChromosome as ReversalDistanceChromosome;
                var bestFitness = chr.Fitness.Value;

                if (chr.FoundSolution)
                {
                    if (chr.SwapToZero < bestSolution)
                    {
                        bestSolution = chr.SwapToZero;
                    }
                }
                latestFitness = bestFitness;
            };

            ga.Start();
            return bestSolution;
        }
    }

    /// <summary>
    /// Chromosome for swap points
    /// </summary>
    public class ReversalDistanceChromosome : ChromosomeBase
    {
        public int SwapToZero { get; internal set; }
        public bool FoundSolution { get; internal set; }

        public ReversalDistanceChromosome(int reversalCount) : base(reversalCount * 2)
        {
            for (int i = 0; i < this.Length; i++)
            {
                ReplaceGene(i, GenerateGene(i));
            }
        }

        public override IChromosome CreateNew()
        {
            return new ReversalDistanceChromosome(ReversalDistance.MaxReversals); // maxReversalsCount count;
        }

        public override Gene GenerateGene(int geneIndex)
        {
            return new Gene(RandomizationProvider.Current.GetInt(0, 10)); // hodnoty v rozsahu 0-9 - indexy do pola pre body swapu
        }
    }

    /// <summary>
    /// Fitness funcntion to evaluate chromosome in Reversal Distance problem
    /// </summary>
    public class ReversalDistanceFitness : IFitness
    {

        int[][] arrs;
        List<int> lstRevs;
        Dictionary<int, int> dctIndexOfNumber;

        public ReversalDistanceFitness(int[] arr0, int[] arr1, Dictionary<int, int> dct)
        {
            arrs = new int[2][];
            arrs[0] = arr0;
            arrs[1] = arr1.Clone() as int[];
            lstRevs = new List<int>();
            dctIndexOfNumber = dct;
        }

        public double Evaluate(IChromosome chromosome)
        {
            var chr = chromosome as ReversalDistanceChromosome;
            var genes = chromosome.GetGenes();
            double fitness = 0.0;
            double swapsToZero = 0;
            lstRevs.Clear();
            lstRevs.AddRange(arrs[1]); // reset
            bool foundSolution = false;
            for (int i = 0; i < chr.Length; i += 2)
            {
                // get start and end poin of swap from genes
                int iStart = (int)genes[i].Value;
                int iEnd = (int)genes[i + 1].Value;
                var iMin = Math.Min(iStart, iEnd);
                var iMax = Math.Max(iStart, iEnd);

                if (iStart != iEnd)
                {
                    while (iMin < iMax)
                    {
                        // swap
                        var c = lstRevs[iMin];
                        lstRevs[iMin] = lstRevs[iMax];
                        lstRevs[iMax] = c;
                        iMin++; iMax--;
                    }
                    swapsToZero++;
                    foundSolution = Enumerable.SequenceEqual(arrs[0], lstRevs);
                    if (foundSolution)
                    {
                        break;
                    }
                }
            }

            // calculate fitness
            if (foundSolution)
            {
                fitness = swapsToZero;
            }
            else
            {
                double diffSum = 0;

                for (int j = 0; j < arrs[0].Length; j++)
                {
                    if (arrs[0][j] != lstRevs[j])
                    {
                        var correctPos = dctIndexOfNumber[lstRevs[j]];
                        diffSum += Math.Pow(Math.Abs(j - correctPos), 2); // suma vzdialenosot od spravnej pozicie
                    }
                }
                fitness = diffSum + swapsToZero + ReversalDistance.NoSolutionPenalty;
            }

            chr.SwapToZero = (int)swapsToZero;
            chr.FoundSolution = foundSolution;

            return 1.0 / fitness;
        }
    }

    public class Asd
    {
        public string Str1 { get; set; }
        public string Str2 { get; set; }
        public int InputID { get; set; }
        public DateTime Start { get; internal set; }
    }



}