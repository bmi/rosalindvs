﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class EnumeratingOrientedGeneOrderings
    {
        public EnumeratingOrientedGeneOrderings()
        {
            var input = 5;
            var inputArray = Enumerable
                .Range(-input, input + input + 1)
                .Where(n => n != 0)
                .ToArray();
            var resultArray = GetPermutations(inputArray, input)
                .Select(r => r.ToList()).Select(i => string.Join(" ", i))
                .ToArray();
            var result = $"{resultArray.Count()}{Environment.NewLine}"
                + string.Join(Environment.NewLine, resultArray);
        }

        IEnumerable<IEnumerable<int>> GetPermutations(IEnumerable<int> list, int length)
        {
            if (length == 1) return list.Select(t => new int[] { t });
            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Any(x => Math.Abs(x) == Math.Abs(e))),
                    (t1, t2) => t1.Concat(new int[] { t2 }));
        }
    }
}