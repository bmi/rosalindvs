﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace RosalindVS
{
    internal class PartialPermutations
    {
        public PartialPermutations()
        {
            int n = 21;
            int k = 7;
            var result = (Factorial(n) / Factorial(n - k)) % 1000000;
        }

        public BigInteger Factorial(BigInteger f)
        {
            if (f == 0)
                return 1;
            else
                return f * Factorial(f - 1);
        }
    }
}