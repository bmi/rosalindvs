﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class EnumeratingKMersLexicographically
    {
        public EnumeratingKMersLexicographically()
        {
            var lst = "A B C D E F G".Split(' ');
            var results = new StringBuilder();
            int L = 3;
            GetPermutations(lst, L, results);

            var result = string.Join(Environment.NewLine,
                results
                .ToString()
                .Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .OrderBy(s => s));
        }

        void GetPermutations<T>(T[] data, int L, StringBuilder res)
        {
            int[] arr = Enumerable.Range(0, data.Length).ToArray();
            int arrLen = arr.Length;
            for (int i = 0; i < (int)Math.Pow(arrLen, L); i++)
            {
                var n = i;
                for (int x = 0; x < L; x++)
                {
                    res.Append(data[n % arrLen]);
                    n /= arrLen;
                }
                res.AppendLine();
            }
        }
    }
}