﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class KMerComposition
    {
        public KMerComposition()
        {
            var input = LoadFastaInput(@"h:\Downloads\rosalind_kmer.txt").First().Value;
            IEnumerable<char> chars = new char[] { 'A', 'C', 'G', 'T' };
            var str = new StringBuilder();

            // occurences for every combination with repetition
            foreach (var perm in CombinationsWithRepetition(chars, 4))
                str.Append($"{Occurences(input, perm)} ");
            var result = str.ToString(); // result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

        public static IEnumerable<String> CombinationsWithRepetition(IEnumerable<char> input, int length)
        {
            if (length <= 0)
                yield return "";
            else
            {
                foreach (var i in input)
                    foreach (var c in CombinationsWithRepetition(input, length - 1))
                        yield return i.ToString() + c;
            }
        }

        public static int Occurences(string source, string substring)
        {
            int count = 0, n = 0;
            if (substring != "")
            {
                while ((n = source.IndexOf(substring, n, StringComparison.InvariantCulture)) != -1)
                {
                    n++;
                    ++count;
                }
            }
            return count;
        }

    }
}