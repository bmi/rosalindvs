﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace RosalindVS
{
    public enum NodeType { AU, CG };
    public static class Tools
    {

        public static Dictionary<string, string> GetRNACodonTable()
        {
            var source = @"UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G";
            var elms = source
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();
            var chunks2 = Enumerable.Range(0, elms.Length / 2)
                .Select(idx => new string[] { elms[idx * 2], elms[idx * 2 + 1] })
                .ToArray();
            var dct = chunks2
                .ToDictionary(k => k[0], v => v[1]);
            return dct;
        }

        public static Dictionary<string, string> GetDNACodonTable()
        {
            var source = @"TTT F      CTT L      ATT I      GTT V
TTC F      CTC L      ATC I      GTC V
TTA L      CTA L      ATA I      GTA V
TTG L      CTG L      ATG M      GTG V
TCT S      CCT P      ACT T      GCT A
TCC S      CCC P      ACC T      GCC A
TCA S      CCA P      ACA T      GCA A
TCG S      CCG P      ACG T      GCG A
TAT Y      CAT H      AAT N      GAT D
TAC Y      CAC H      AAC N      GAC D
TAA Stop   CAA Q      AAA K      GAA E
TAG Stop   CAG Q      AAG K      GAG E
TGT C      CGT R      AGT S      GGT G
TGC C      CGC R      AGC S      GGC G
TGA Stop   CGA R      AGA R      GGA G
TGG W      CGG R      AGG R      GGG G";
            var elms = source
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();
            var chunks2 = Enumerable.Range(0, elms.Length / 2)
                .Select(idx => new string[] { elms[idx * 2], elms[idx * 2 + 1] })
                .ToArray();
            var dct = chunks2
                .ToDictionary(k => k[0], v => v[1]);
            return dct;
        }


        static char[] au = { 'A', 'U' };
        static char[] cg = { 'C', 'G' };
        internal static char[] GetCharsType(NodeType nt)
        {
            switch (nt)
            {
                case NodeType.AU:
                    return au;

                case NodeType.CG:
                    return cg;

                default:
                    throw new NotImplementedException();
            }
        }

        internal static NodeType GetNodeType(Node n)
        {
            if (n.Char == 'A') return NodeType.AU;
            if (n.Char == 'U') return NodeType.AU;
            if (n.Char == 'C') return NodeType.CG;
            if (n.Char == 'G') return NodeType.CG;
            throw new NotImplementedException();
        }

        public static string GetRevComplement(string src)
        {
            char[] arr = src.ToCharArray();
            Array.Reverse(arr);
            var sRevCompl = new string(arr);
            sRevCompl = sRevCompl.Replace('A', 'a').Replace('C', 'c');
            sRevCompl = sRevCompl.Replace('T', 'A').Replace('G', 'C');
            sRevCompl = sRevCompl.Replace('a', 'T').Replace('c', 'G');
            return sRevCompl;
        }

        public static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }

        public static int HammingDistance(string str1, string str2)
        {
            var result = 0;
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i] != str2[i])
                    result++;
            }
            return result;
        }



        public static string DNA2RNA(string dna)
        {
            return dna.Replace('T', 'U');
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }


        public static Dictionary<int, BigInteger> dctCat = new Dictionary<int, BigInteger>();
        public static BigInteger catalan(int n)
        {
            if (dctCat.ContainsKey(n))
                return dctCat[n];
            BigInteger result = 0;
            if (n <= 1)
                return (BigInteger)1;
            for (int i = 0; i < n; i++)
                result += catalan(i) * catalan(n - i - 1);
            dctCat.Add(n, result);
            return result;
        }


        public static IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });
            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => !t.Contains(e)),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }

        public static IEnumerable<String> CombinationsWithRepetition(IEnumerable<char> input, int length)
        {
            if (length <= 0)
                yield return "";
            else
            {
                foreach (var i in input)
                    foreach (var c in CombinationsWithRepetition(input, length - 1))
                        yield return i.ToString() + c;
            }
        }

        public static int Occurences(string source, string substring)
        {
            int count = 0, n = 0;
            if (substring != "")
            {
                while ((n = source.IndexOf(substring, n, StringComparison.InvariantCulture)) != -1)
                {
                    n++;
                    ++count;
                }
            }
            return count;
        }

        public static int[] OccurencesList(string source, char substring)
        {
            List<int> lst = new List<int>();
            int count = 0, n = 0;

            lst.Clear();
            while ((n = source.IndexOf(substring, n)) != -1)
            {
                lst.Add(n++);
                ++count;
            }
            return lst.ToArray();
        }

        public static int[] OccurencesList(string source, char substring, int minIndex)
        {
            List<int> lst = new List<int>();
            int count = 0, n = 0;

            lst.Clear();
            while ((n = source.IndexOf(substring, n)) != -1)
            {
                if (n < minIndex)
                {
                    n++;
                    continue;
                }
                lst.Add(n++);
                ++count;
            }
            return lst.ToArray();
        }

        private static IEnumerable<int[]> CombinationsRosettaWoRecursion(int m, int n)
        {
            int[] result = new int[m];
            Stack<int> stack = new Stack<int>(m);
            stack.Push(0);
            while (stack.Count > 0)
            {
                int index = stack.Count - 1;
                int value = stack.Pop();
                while (value < n)
                {
                    result[index++] = value++;
                    stack.Push(value);
                    if (index != m) continue;
                    yield return (int[])result.Clone(); // thanks to @xanatos
                                                        //yield return result;
                    break;
                }
            }
        }

        /// <summary>
        /// Have to be use in foreach - otherwise not work
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static IEnumerable<T[]> Combinations<T>(T[] array, int m)
        {
            if (array.Length < m)
                throw new ArgumentException("Array length can't be less than number of selected elements");
            if (m < 1)
                throw new ArgumentException("Number of selected elements can't be less than 1");
            T[] result = new T[m];
            foreach (int[] j in CombinationsRosettaWoRecursion(m, array.Length))
            {
                for (int i = 0; i < m; i++)
                {
                    result[i] = array[j[i]];
                }
                yield return result;
            }
        }

        public static BigInteger CombinationsCount(int n, int r)
        {
            return Factorial(n) / (Factorial(r) * Factorial(n - r));
        }

        static Dictionary<int, BigInteger> dctFactorial = new Dictionary<int, BigInteger>();
        public static BigInteger Factorial(int i)
        {
            if (i <= 1)
                return 1;
            if (!dctFactorial.ContainsKey(i))
                dctFactorial.Add(i, i * Factorial(i - 1));
            return dctFactorial[i];
        }

        public static Tuple<TimeSpan, TimeSpan> TestPerf(Action a, Action b, int count)
        {
            var swa = new Stopwatch();
            var swb = new Stopwatch();
            for (int i = 0; i < 2; i++)
            {
                swa.Start();
                for (int j = 0; j < count >> 1; j++)
                {
                    a();
                }
                swa.Stop();
                swb.Start();
                for (int j = 0; j < count >> 1; j++)
                {
                    b();
                }
                swb.Stop();
            }
            return new Tuple<TimeSpan, TimeSpan>(swa.Elapsed, swb.Elapsed);
        }

    }

}


