﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Reinsertions;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Configuration;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RosalindVS
{
    public enum GAOSelection { EliteSelection, RouletteWheelSelection, StochasticUniversalSamplingSelection, TournamentSelection };

    public enum GAOCrossover
    {
        UniformCrossover, CycleCrossover, OnePointCrossover, OrderBasedCrossover, OrderedCrossover,
        PartiallyMappedCrossover, PositionBasedCrossover, ThreeParentCrossover, TwoPointCrossover,
        VotingRecombinationCrossover
    }

    public enum GAOMutation
    {
        UniformMutation, TworsMutation, DisplacementMutation, FlipBitMutation, InsertionMutation,
        PartialShuffleMutation, ReverseSequenceMutation,
        Custom
    }

    public enum GAOReinsertion
    {
        ElitistReinsertion,
        FitnessBasedReinsertion,
        PureReinsertion,
        UniformReinsertion
    }

    public class GAEngineOpt
    {


        private Func<IChromosome> getChromosome;
        private Func<IFitness> getFitness;
        private Func<IMutation> getMutation;
        Func<IChromosome, string> getChrInterpretation;

        public List<GAOptResult> OptResultList { get; private set; }
        public object BestResponse { get; private set; }

        public GAEngineOpt(Func<IChromosome> getChromosome,
            Func<IFitness> getFitness,
            Func<IMutation> getMutation,
            Func<IChromosome, string> getChromsString
            )
        {
            this.getChromosome = getChromosome;
            this.getFitness = getFitness;
            this.getMutation = getMutation;
            this.getChrInterpretation = getChromsString;
        }

        public void Start()
        {
            var selection = new EliteSelection();
            var crossover = new UniformCrossover();
            var mutation = new TworsMutation();
            var population = new Population(50, 150, getChromosome());

            var ga = new GeneticAlgorithm(population, getFitness(), selection, crossover, mutation);
            ga.Termination = new FitnessStagnationTermination(100);
            ga.Start();
        }



        public void Optimize(GAOpts gaOpts)
        {
            File.Delete("gaopt.txt");
            OptResultList = new List<GAOptResult>();
            GAOptResult totalBest = new GAOptResult();
            totalBest.BestFitness = 0;
            var lsels = gaOpts.Selections;
            if (!lsels.Any()) lsels.AddRange(Enum.GetValues(typeof(GAOSelection)).OfType<GAOSelection>());

            var lcross = gaOpts.Crossovers;
            if (!lcross.Any()) lcross.AddRange(Enum.GetValues(typeof(GAOCrossover)).OfType<GAOCrossover>());

            var lmuts = gaOpts.Mutations;
            if (!lmuts.Any()) lmuts.AddRange(Enum.GetValues(typeof(GAOMutation)).OfType<GAOMutation>());

            var lrein = gaOpts.Reinsertions;
            if (!lrein.Any()) lrein.AddRange(Enum.GetValues(typeof(GAOReinsertion)).OfType<GAOReinsertion>());


            ISelection selection;
            foreach (var eSelection in lsels)
            {
                #region selection
                switch (eSelection)
                {
                    case GAOSelection.EliteSelection:
                        selection = new EliteSelection();
                        break;
                    case GAOSelection.RouletteWheelSelection:
                        selection = new RouletteWheelSelection();
                        break;
                    case GAOSelection.StochasticUniversalSamplingSelection:
                        selection = new StochasticUniversalSamplingSelection();
                        break;
                    case GAOSelection.TournamentSelection:
                        selection = new TournamentSelection();
                        break;
                    default:
                        throw new NotImplementedException();
                }
                #endregion

                ICrossover crossover;
                foreach (var eCrossover in lcross)
                {
                    if (gaOpts.CrossoversExcl.Contains(eCrossover)) continue;
                    #region crossover
                    switch (eCrossover)
                    {
                        case GAOCrossover.UniformCrossover:
                            crossover = new UniformCrossover();
                            break;
                        case GAOCrossover.CycleCrossover:
                            crossover = new CycleCrossover();
                            break;
                        case GAOCrossover.OnePointCrossover:
                            crossover = new OnePointCrossover();
                            break;
                        case GAOCrossover.OrderBasedCrossover:
                            crossover = new OrderBasedCrossover();
                            break;
                        case GAOCrossover.OrderedCrossover:
                            crossover = new OrderedCrossover();
                            break;
                        case GAOCrossover.PartiallyMappedCrossover:
                            crossover = new PartiallyMappedCrossover();
                            break;
                        case GAOCrossover.PositionBasedCrossover:
                            crossover = new PositionBasedCrossover();
                            break;
                        case GAOCrossover.ThreeParentCrossover:
                            crossover = new ThreeParentCrossover();
                            break;
                        case GAOCrossover.TwoPointCrossover:
                            crossover = new TwoPointCrossover();
                            break;
                        case GAOCrossover.VotingRecombinationCrossover:
                            crossover = new VotingRecombinationCrossover();
                            break;
                        default:
                            throw new NotImplementedException();
                    }
                    #endregion

                    IMutation mutation;
                    foreach (var eMutation in lmuts)
                    {
                        #region mutation
                        switch (eMutation)
                        {
                            case GAOMutation.UniformMutation:
                                mutation = new UniformMutation();
                                break;
                            case GAOMutation.TworsMutation:
                                mutation = new TworsMutation();
                                break;
                            case GAOMutation.DisplacementMutation:
                                mutation = new DisplacementMutation();
                                break;
                            case GAOMutation.FlipBitMutation:
                                mutation = new FlipBitMutation();
                                break;
                            case GAOMutation.InsertionMutation:
                                mutation = new InsertionMutation();
                                break;
                            case GAOMutation.PartialShuffleMutation:
                                mutation = new PartialShuffleMutation();
                                break;
                            case GAOMutation.ReverseSequenceMutation:
                                mutation = new ReverseSequenceMutation();
                                break;
                            case GAOMutation.Custom:
                                if (getMutation != null)
                                    mutation = this.getMutation();
                                else
                                    throw new NotImplementedException("Custom mutation missing");
                                break;
                            default:
                                throw new NotImplementedException();
                        }
                        #endregion

                        IReinsertion reinsertion;
                        foreach (var eReinsertion in lrein)
                        {
                            #region reinsertion
                            switch (eReinsertion)
                            {
                                case GAOReinsertion.ElitistReinsertion:
                                    reinsertion = new ElitistReinsertion();
                                    break;
                                case GAOReinsertion.FitnessBasedReinsertion:
                                    reinsertion = new FitnessBasedReinsertion();
                                    break;
                                case GAOReinsertion.PureReinsertion:
                                    reinsertion = new PureReinsertion();
                                    break;
                                case GAOReinsertion.UniformReinsertion:
                                    reinsertion = new UniformReinsertion();
                                    break;
                                default:
                                    throw new NotImplementedException();
                            }
                            #endregion


                            for (float probCrossover = gaOpts.CrossoverProbMin; probCrossover <= gaOpts.CrossoverProbMax; probCrossover += gaOpts.CrossoverProbStep)
                            {
                                for (float probMut = gaOpts.MutationProbMin; probMut <= gaOpts.MutationProbMax; probMut += gaOpts.MutationProbStep)
                                {
                                    GAOptResult res = new GAOptResult
                                    {
                                        Selection = eSelection,
                                        Crossover = eCrossover,
                                        Mutation = eMutation,
                                        Reinsertion = eReinsertion,
                                        BestFitness = 0,
                                        ProbCrossover = probCrossover,
                                        ProbMutation = probMut
                                    };
                                    OptResultList.Add(res);
                                    res.Info = $"{eSelection}, {eCrossover} {probCrossover}, {eMutation} {probMut}, {eReinsertion}, Pop: {gaOpts.PopulationMin}-{gaOpts.PopulationMax}: ";
                                    Console.Write(res.Info);
                                    Stopwatch sw = new Stopwatch();
                                    sw.Start();
                                    for (int i = 0; i < gaOpts.RepeatCount; i++)
                                    {
                                        var chromosome = getChromosome();
                                        var fitness = getFitness();
                                        var population = new Population(gaOpts.PopulationMin, gaOpts.PopulationMax, chromosome);

                                        try
                                        {
                                            var ga = new GeneticAlgorithm(population, fitness, selection, crossover, mutation);
                                            ga.CrossoverProbability = probCrossover;
                                            ga.MutationProbability = probMut;
                                            ga.Reinsertion = reinsertion;

                                            ga.Termination = new FitnessStagnationTermination(gaOpts.FitnessStagnationTermination);
                                            ga.Start();
                                            if (ga.BestChromosome.Fitness > res.BestFitness)
                                            {
                                                res.BestFitness = ga.BestChromosome.Fitness;
                                                res.BestChromosome = ga.BestChromosome;
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            res.Invalid = true;
                                            res.ExcMessage = ex.Message;
                                            break;
                                        }
                                    }
                                    sw.Stop();
                                    string msg;
                                    if (!res.Invalid)
                                    {
                                        if (res.BestFitness > totalBest.BestFitness)
                                        {
                                            totalBest = res;
                                            if (getChrInterpretation != null)
                                                this.BestResponse = getChrInterpretation(res.BestChromosome);
                                        }
                                        res.DurationSecs = Math.Round(sw.Elapsed.TotalMilliseconds / 1000.0, 1);
                                        if (getChrInterpretation != null)
                                        {
                                            res.BestString = getChrInterpretation(res.BestChromosome);
                                        }
                                        msg = $"Best fitness: {res.BestFitness,10} (max: {totalBest.BestFitness}), {res.DurationSecs,10} s., {res.Invalid}, {res.BestString}";
                                    }
                                    else
                                    {
                                        msg = $"ERROR: {res.ExcMessage}";
                                    }
                                    File.AppendAllText("gaopt.txt", $"{res.Info}: {msg}{Environment.NewLine}");
                                    Console.WriteLine(msg);
                                }
                            }
                        }
                    }
                }
            }
            Console.WriteLine("Done");
        }

    }

    public class GAOptResult
    {
        public GAOSelection Selection { get; set; }
        public GAOCrossover Crossover { get; set; }
        public GAOMutation Mutation { get; set; }

        public double DurationSecs { get; set; }
        public IChromosome BestChromosome { get; set; }

        public bool Invalid { get; set; }
        public string ExcMessage { get; internal set; }
        public double? BestFitness { get; internal set; }
        public string BestString { get; internal set; }
        public double ProbCrossover { get; internal set; }
        public double ProbMutation { get; internal set; }
        public GAOReinsertion Reinsertion { get; internal set; }
        public string Info { get; internal set; }
    }

    public class GAOpts
    {
        public List<GAOSelection> Selections { get; private set; }
        public List<GAOCrossover> Crossovers { get; private set; }
        public List<GAOCrossover> CrossoversExcl { get; private set; }
        public List<GAOMutation> Mutations { get; private set; }
        public List<GAOReinsertion> Reinsertions { get; private set; }


        public int PopulationMin { get; set; } = 50;
        public int PopulationMax { get; set; } = 100;

        /// <summary>
        /// Default 10
        /// </summary>
        public int RepeatCount { get; set; } = 10;

        /// <summary>
        /// Default 100
        /// </summary>
        public int FitnessStagnationTermination { get; set; } = 100;


        /// <summary>
        /// Default: 0.75
        /// </summary>
        public float CrossoverProbMin { get; set; } = 0.75f;

        /// <summary>
        /// Default: 0.75
        /// </summary>
        public float CrossoverProbMax { get; set; } = 0.75f;
        /// <summary>
        /// Default: 0.1
        /// </summary>
        public float CrossoverProbStep { get; set; } = 0.1f;

        /// <summary>
        /// Default: 0.1
        /// </summary>
        public float MutationProbMin { get; set; } = 0.1f;
        /// <summary>
        /// Default: 0.1
        /// </summary>
        public float MutationProbMax { get; set; } = 0.1f;
        /// <summary>
        /// Default: 0.1
        /// </summary>
        public float MutationProbStep { get; set; } = 0.1f;


        public GAOpts()
        {
            Selections = new List<GAOSelection>();
            Crossovers = new List<GAOCrossover>();
            CrossoversExcl = new List<GAOCrossover>();
            Mutations = new List<GAOMutation>();
            Reinsertions = new List<GAOReinsertion>();
        }
    }

}


