﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;

namespace RosalindVS
{
    internal class FindingAProteinMotif
    {
        public FindingAProteinMotif()
        {
            string[] inputIDs =
            {
                "A2Z669",
                "B5ZC00",
                "P07204_TRBM_HUMAN",
                "P20840_SAG1_YEAST"
            };
            var url = "https://www.uniprot.org/uniprot/";
            var sbInput = new StringBuilder();
            using (var client = new WebClient())            // getting data from web
            {
                foreach (var id in inputIDs)
                {
                    var str = client.DownloadString($"{url}{id}.fasta");
                    sbInput.AppendLine(str);
                }
            }
            var lines = getFASTAData(sbInput.ToString()).ToArray();

            // glycosylationMotif = "N{P}[ST]{P}";
            var pos = new List<int>();
            var sbResult = new StringBuilder();
            var idx = 0;
            foreach (var line in lines)
            {
                pos.Clear();
                for (int i = 0; i < line.Item2.Length - 4; i++)
                {
                    if (line.Item2[i] == 'N'                        // testing possessing the N-glycosylation motif
                      && line.Item2[i + 1] != 'P'
                      && (line.Item2[i + 2] == 'S' || line.Item2[i + 2] == 'T')
                      && line.Item2[i + 3] != 'P'
                      )
                    {
                        pos.Add(i + 1);
                    }
                }
                if (pos.Any())
                {
                    sbResult.AppendLine(inputIDs[idx]);
                    sbResult.AppendLine(string.Join(" ", pos));
                }
                idx++;
            }
            var result = sbResult.ToString(); // result here


            // getting collection from string, Item1=Name. Item2=data
            IEnumerable<Tuple<string, string>> getFASTAData(string strFASTA)
            {
                return strFASTA
                    .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => str.Split(new string[] { Environment.NewLine, "\n" }, StringSplitOptions.RemoveEmptyEntries))
                    .Select(strArr => new Tuple<string, string>(strArr[0].Trim(), string.Join("", strArr.Skip(1)).Trim()));
            }

        }
    }
}