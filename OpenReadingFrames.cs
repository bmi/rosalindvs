﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using c = System.Console;

namespace RosalindVS
{
    internal class OpenReadingFrames
    {
        public OpenReadingFrames()
        {
            // Load data
            var input = LoadFastaInput(@"h:\Downloads\rosalind_orf.txt").First();
            var openDNA = "ATG";
            var stopDNA = new string[] { "TAG", "TGA", "TAA" };

            // DNA
            var open = openDNA;
            var stops = stopDNA;
            var dctTable = GetDNACodonTable();
            var dctResults = new HashSet<string>();

            var lstBase = new Dictionary<string, string>();
            // add DNA string
            lstBase.Add("DNA", input.Value);

            // add reverse complement
            lstBase.Add("DNARC", GetRevComplement(input.Value));

            // create shifted variants
            var lstVariants = new Dictionary<string, string>();
            foreach (var item in lstBase)
            {
                for (int shift = 0; shift < 3; shift++)
                {
                    var str = item.Value.Substring(shift);
                    lstVariants.Add($"{item.Key}_{shift}", str);
                }
            }

            // get data from strings
            foreach (var item in lstVariants)
            {
                if (string.IsNullOrEmpty(item.Value))
                    continue;

                // split to 3 chars parts
                var p3 = Split(item.Value, 3).ToArray();

                // translate
                var tran = string.Join("", p3.Select(p => dctTable[p]));

                // exclude string without Stop
                if (!tran.Contains("Stop"))
                    continue;

                // remove string after last Stop
                tran = tran.Substring(0, tran.LastIndexOf("Stop") + 4);

                // create parts ended with Stop
                var stopParts = tran.Split(new string[] { "Stop" }, StringSplitOptions.None);
                foreach (var itemx in stopParts)
                {
                    if (itemx.Contains('M')) // valid is only string contains starting char M
                    {
                        var str = itemx.Substring(itemx.IndexOf("M"));
                        var mindex = 0;

                        while (true) // each substring with M need to add to results
                        {
                            if (!dctResults.Contains(str)) // but distinct
                                dctResults.Add(str);
                            mindex = str.IndexOf('M', mindex + 1); // find another string starting with M
                            if (mindex > -1)
                            {
                                str = str.Substring(mindex);
                                mindex = 0;
                            }
                            else
                                break;
                        }
                    }
                }
            }

            var sb = new StringBuilder();
            foreach (var item in dctResults)
                sb.AppendLine($"{item}");
            var result = sb.ToString(); // result here
        }

        /// <summary>
        /// Load fasta data
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

        public static Dictionary<string, string> GetDNACodonTable()
        {
            var source = @"TTT F      CTT L      ATT I      GTT V
TTC F      CTC L      ATC I      GTC V
TTA L      CTA L      ATA I      GTA V
TTG L      CTG L      ATG M      GTG V
TCT S      CCT P      ACT T      GCT A
TCC S      CCC P      ACC T      GCC A
TCA S      CCA P      ACA T      GCA A
TCG S      CCG P      ACG T      GCG A
TAT Y      CAT H      AAT N      GAT D
TAC Y      CAC H      AAC N      GAC D
TAA Stop   CAA Q      AAA K      GAA E
TAG Stop   CAG Q      AAG K      GAG E
TGT C      CGT R      AGT S      GGT G
TGC C      CGC R      AGC S      GGC G
TGA Stop   CGA R      AGA R      GGA G
TGG W      CGG R      AGG R      GGG G";
            var elms = source
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();
            var chunks2 = Enumerable.Range(0, elms.Length / 2)
                .Select(idx => new string[] { elms[idx * 2], elms[idx * 2 + 1] })
                .ToArray();
            var dct = chunks2
                .ToDictionary(k => k[0], v => v[1]);
            return dct;
        }

        public static string GetRevComplement(string src)
        {
            char[] arr = src.ToCharArray();
            Array.Reverse(arr);
            var sRevCompl = new string(arr);
            sRevCompl = sRevCompl.Replace('A', 'a').Replace('C', 'c');
            sRevCompl = sRevCompl.Replace('T', 'A').Replace('G', 'C');
            sRevCompl = sRevCompl.Replace('a', 'T').Replace('c', 'G');
            return sRevCompl;
        }

        public static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }
    }
}