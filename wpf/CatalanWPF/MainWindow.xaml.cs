﻿using RosalindVS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CatalanWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        NodeLists lists;
        List<Line> lstLines = new List<Line>();
        static Dictionary<Tuple<int, int, int, int>, bool> dctIsCrossed = new Dictionary<Tuple<int, int, int, int>, bool>();
        static HashSet<string> HSUniqSols = new HashSet<string>();
        bool waitDialog = false;

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            string input = "";
            bool useEllipse;
            input = "AUAUAUAUAU"; useEllipse = false;
            input = "UAGCGUGAUCAC"; useEllipse = true;
            input = "AUUCUAUACCGGCGGAUAUGCAAAGGCCCGUCUAGUAUAAAAGCUUACGACGUUAUGCGCUAUUAAUAGGCCGUACAUCGGCCGCGCGCGGCGGCGCUCUAUACUAGGAUAUACACGCGUGGGCCCUAGACGUCUGAAGUACUUCCGGCGCCUAGUAUGCAUUAGCAUACGUAUGCGCGCUGGUACGUACCGCAUAUAUAGUACGCUAUAUGCCGCUAGAUCGCAUGUAAUAUAAAUAUUCGCGGC"; useEllipse = true;
            input = "GGCUGCUACGCGUAAGCCGGCUGCUACGCGUAAGCC"; useEllipse = false;
            var cat2 = new Cat2(input);
            draw(input, useEllipse);



            // draw("AUUCUAUACCGGCGGAUAUGCAAAGGCCCGUCUAGUAUAAAAGCUUACGACGUUAUGCGCUAUUAAUAGGCCGUACAUCGGCCGCGCGCGGCGGCGCUCUAUACUAGGAUAUACACGCGUGGGCCCUAGACGUCUGAAGUACUUCCGGCGCCUAGUAUGCAUUAGCAUACGUAUGCGCGCUGGUACGUACCGCAUAUAUAGUACGCUAUAUGCCGCUAGAUCGCAUGUAAUAUAAAUAUUCGCGGC", true);
        }

        #region UI
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }

        private void draw(string input, bool useRadiusX)
        {

            lists = new RosalindVS.NodeLists(input);

            var centerX = ActualWidth / 2;
            var centerY = ActualHeight / 2 - 50;
            var radiusY = centerY - 20;
            var radiusX = radiusY;
            if (useRadiusX)
                radiusX = centerX - 50;

            var pointCount = lists.ListNodes.Count;
            var angle = (2 * Math.PI) / pointCount;
            var a = 0.0;
            foreach (var node in lists.ListNodes)
            {
                node.X = centerX + (radiusX * Math.Cos(a));
                node.Y = centerY + (radiusY * Math.Sin(a));
                var chr = node.Char;
                var tb = new TextBlock { Text = chr.ToString(), FontSize = 14, FontWeight = FontWeights.SemiBold, ToolTip = $"{node.Index}" };
                tb.Tag = node;
                tb.PreviewMouseLeftButtonDown += Tb_PreviewMouseLeftButtonDown;
                if (chr == 'A' || chr == 'U')
                    tb.Foreground = Brushes.Red;
                else
                    tb.Foreground = Brushes.Green;

                Canvas.SetLeft(tb, node.X);
                Canvas.SetTop(tb, node.Y);
                canvas.Children.Add(tb);

                tb = new TextBlock { Text = $"{node.Index}", FontSize = 10 };
                var x = centerX + ((radiusX - 35) * Math.Cos(a));
                var y = centerY + ((radiusY - 35) * Math.Sin(a));

                Canvas.SetLeft(tb, x);
                Canvas.SetTop(tb, y);
                canvas.Children.Add(tb);

                tb = new TextBlock { Text = $"#{node.ValidConnections.Count}", FontSize = 10 };
                x = centerX + ((radiusX - 70) * Math.Cos(a));
                y = centerY + ((radiusY - 70) * Math.Sin(a));

                Canvas.SetLeft(tb, x);
                Canvas.SetTop(tb, y);
                canvas.Children.Add(tb);


                a += angle;
            }

            tests();
        }

        private void tests()
        {
            var a = isCrossed(33, 2, 31, 3); // ok false
            a = isCrossed(31, 3, 33, 4);
            a = isCrossed(20, 27, 22, 29);
        }

        private void Tb_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            clearLines();
            var tb = sender as TextBlock;
            var node = tb.Tag as Node;
            foreach (var node2 in node.ValidConnections)
            {
                addLine(node, node2);
            }
        }

        private void addLine(Node node, Node node2)
        {
            var line = new Line();
            line.X1 = node.X + 10;
            line.Y1 = node.Y + 10;
            line.X2 = node2.X + 10;
            line.Y2 = node2.Y + 10;
            line.Stroke = Brushes.Gray;
            line.StrokeThickness = 1;
            line.StrokeDashArray = new DoubleCollection();
            line.StrokeDashArray.Add(2);
            line.StrokeDashArray.Add(4);
            canvas.Children.Add(line);
            lstLines.Add(line);
        }

        private void clearLines()
        {
            foreach (var l in lstLines)
            {
                canvas.Children.Remove(l);
            }
            lstLines.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            clearLines();
            var conn0 = lists.GetNodesWithXConn(0);
            var conn1 = lists.GetNodesWithXConn(1);
            var conn2 = lists.GetNodesWithXConn(2);
            foreach (var item in conn1)
            {
                addLine(item, item.ValidConnections[0]);
            }
        }
        #endregion UI

        int sols = 0;
        private void sol_Click(object sender, RoutedEventArgs e)
        {
            dctIsCrossed.Clear();
            HSUniqSols.Clear();
            var lst = lists.ListNodes;
            var sw = new Stopwatch();
            sw.Start();
            foreach (var startNode in lst)
            {
                foreach (var conn in startNode.ValidConnections)
                {
                    var oc = new OneCombination(this.lists);
                    if (oc.IsValidNodes(startNode.Index, conn.Index))
                    {
                        var tpl = getTuple(startNode.Index, conn.Index);
                        oc.HsConnsAdd(tpl);
                        getNextConns(oc, startNode);
                    }
                }
            }
            sw.Stop();
            Debug.WriteLine($"Solution duration: {sw.ElapsedMilliseconds} ms");
            Debug.WriteLine($"Solution count: {HSUniqSols.Count}");
            Debug.WriteLine($"Tuples created: {tplCount}");

            var sums = this
                .lstSoutions
                .Select(sol => new { sol = sol, sum = sol.HsConns().Sum(r => getDist(r)) })
                .OrderByDescending(i => i.sum);
            drawSol(sums.First().sol);

        }

        int getDist(Tuple<int, int> conn)
        {
            if (Math.Abs(conn.Item1 - conn.Item2) == 1)
                return 0;
            return 1;
            //if (conn.Item1 < conn.Item2)
            //    return conn.Item2 - conn.Item1;
            //else
            //    return conn.Item2 + this.lists.NodesCount - conn.Item1;
        }

        Dictionary<int, Tuple<int, int>> dctTuples = new Dictionary<int, Tuple<int, int>>();
        int tplCount;
        private readonly List<OneCombination> lstSoutions = new List<OneCombination>();

        Tuple<int, int> getTuple(int i1, int i2)
        {
            var key = i1 * 100000 + i2;
            if (dctTuples.ContainsKey(key))
                return dctTuples[key];
            tplCount++;
            var tpl = new Tuple<int, int>(i1, i2);
            dctTuples.Add(key, tpl);
            return tpl;
        }

        private void getNextConns(OneCombination oc, Node startNode)
        {
            var nodeNext = startNode.Next;
            while (oc.HsUsedNodesContains(nodeNext.Index) && !nodeNext.Equals(startNode))
                nodeNext = nodeNext.Next; // new not used node or end

            if (!nodeNext.Equals(startNode))
            {
                bool first = true;
                int firstIndex = -1;
                foreach (var n2 in nodeNext.ValidConnections)
                {
                    if (oc.HsUsedNodesContains(n2.Index)) continue;
                    // nodeNext a n2 su dva nove nepouzite connectiony

                    bool isCross = false;
                    foreach (var conn in oc.HsConns())
                    {
                        if (isCrossed(conn.Item1, conn.Item2, nodeNext.Index, n2.Index))
                        {
                            isCross = true;
                            break;
                        }
                    }
                    if (isCross) continue;
                    if (first)
                    {
                        first = false;
                        firstIndex = n2.Index;
                    }
                    else
                    {
                        if (oc.IsValidNodes(nodeNext.Index, n2.Index))
                        {
                            // new tree
                            var newOc = oc.Duplicate();
                            var newConn = getTuple(nodeNext.Index, n2.Index);
                            newOc.HsConnsAdd(newConn);
                            getNextConns(newOc, nodeNext); // rekurzivne dohladavanie
                        }

                    }
                    if (oc.IsValidNodes(nodeNext.Index, firstIndex))
                    {
                        // continue with oc
                        var newConn2 = getTuple(nodeNext.Index, firstIndex);
                        oc.HsConnsAdd(newConn2);
                        getNextConns(oc, nodeNext); // rekurzivne dohladavanie
                    }
                }
            }
            else
            {
                var conns = oc.HsConns();
                if (conns.Count == lists.HalfNodesCount)
                {

                    var strKey = oc.GetNormalizedStr();
                    if (!HSUniqSols.Contains(strKey))
                    {
                        sols++;
                        HSUniqSols.Add(strKey);
                        lstSoutions.Add(oc);
                        oc.GroupIt();
                        if (waitDialog)
                            drawSol(oc);
                    }
                    else
                    {

                    }
                }
                else
                {
                    // invalid sol
                }
            }


        }

        private void drawSol(OneCombination oc)
        {
            clearLines();
            var sb = new StringBuilder();
            sb.AppendLine();
            var idx = 0;
            foreach (var item in oc.HsConns())
            {
                var n1 = lists.DctNodes[item.Item1];
                var n2 = lists.DctNodes[item.Item2];
                addLine(n1, n2);
                sb.AppendLine($"{idx++}: {item.Item1}-{item.Item2}");
            }
            MessageBox.Show(oc.ToString());
        }

        private bool isCrossed(int i, int j, int k, int l)
        {
            var key = new Tuple<int, int, int, int>(i, j, k, l);
            if (dctIsCrossed.ContainsKey(key))
            {
                return dctIsCrossed[key];
            }
            var _i = i;
            var _j = j;
            if (j < i)
                _j += lists.NodesCount;
            var _k = k;
            var _l = l;
            if (l < k)
                _l += lists.NodesCount;

            if (Math.Abs(_i - _j) == 1 || Math.Abs(_k - _l) == 1)
            {
                dctIsCrossed.Add(key, false);
                return false;
            }

            bool x = _i < _k && _k < _j && _j < _l;
            dctIsCrossed.Add(key, x);
            return x;
        }
    }

    public class OneCombination
    {
        List<int> hsUsedNodes { get; set; }
        List<Tuple<int, int>> lstConns { get; set; }
        NodeLists lists;
        List<List<int>> lstGrps;
        internal BigInteger CatalanNumber { get; private set; } = 1;

        public int NodesCount { get; private set; }
        public string StrIndexes { get; private set; }
        public string StrConns { get; private set; }
        public string StrConns2 { get; private set; }

        public OneCombination(NodeLists lists)
        {
            this.lists = lists;
            hsUsedNodes = new List<int>();
            lstConns = new List<Tuple<int, int>>();
        }

        internal OneCombination Duplicate()
        {
            var newoc = new OneCombination(this.lists);
            newoc.hsUsedNodes.AddRange(hsUsedNodes);
            newoc.lstConns.AddRange(lstConns);
            return newoc;
        }

        internal void GroupIt()
        {
            this.lstGrps = new List<List<int>>();
            this.CatalanNumber = 1;
            var sb = new StringBuilder();
            var sb2 = new StringBuilder();

            foreach (var conn in lstConns)
            {
                sb.AppendLine($"{conn.Item1}-{conn.Item2}");
                if (Math.Abs(conn.Item1 - conn.Item2) > 1)
                    sb2.AppendLine($"{conn.Item1}-{conn.Item2}");
                addToGroup(conn);
            }
            this.StrConns = sb.ToString();
            this.StrConns2 = sb2.ToString();
            sb.Clear(); sb2.Clear();
            var ptsCount = lstGrps.Sum(g => g.Count);
            if (ptsCount != lists.NodesCount)
            {

            }
            else
            {

                foreach (var g in lstGrps)
                {
                    sb.AppendLine($"{string.Join(", ", g)}");
                    sb.AppendLine($"{string.Join("", g.Select(c => lists.DctNodes[c].Char))}");
                    if (g.Count == 2) continue;
                    CatalanNumber = CatalanNumber * Tools.catalan(g.Count >> 1);
                }
                this.StrIndexes = sb.ToString();
                // this.StrChars = sb2.ToString();
            }
        }

        private void addToGroup(Tuple<int, int> conn)
        {
            // najdi alebo vytvor skupinu
            var grp = getAdjacentGroup(conn.Item1);
            if (grp == null)
                grp = getAdjacentGroup(conn.Item2);
            if (grp == null)
            {
                grp = new List<int>();
                lstGrps.Add(grp);
            }

            grp.Add(conn.Item1);
            grp.Add(conn.Item2);
        }

        private List<int> getAdjacentGroup(int idx)
        {
            var idxChar = lists.DctNodes[idx].Char;
            int adjIdx = idx - 1;
            if (adjIdx < 0)
                adjIdx = lists.NodesCount - 1;
            var grp = lstGrps.FirstOrDefault(g => g.Contains(adjIdx));
            if (grp == null)
            {
                adjIdx = idx + 1;
                if (adjIdx == lists.NodesCount)
                    adjIdx = 0;
                grp = lstGrps.FirstOrDefault(g => g.Contains(adjIdx));
            }

            // kontrola rovnaej skupiny
            if (grp != null)
            {
                var n1 = lists.DctNodes[grp.First()];
                var n2 = lists.DctNodes[idx];
                if (n1.NodeType != n2.NodeType)
                {
                    grp = null;
                }
            }
            return grp;
        }

        public override string ToString()
        {

            return $@"
Connections:
{StrConns}
---
Connections > 1:
{StrConns2}
---
Catalan: {CatalanNumber}
---
Indexy: 
{StrIndexes}
---
";

        }

        internal List<Tuple<int, int>> HsConns()
        {
            return lstConns;
        }

        internal void HsConnsAdd(Tuple<int, int> newConn)
        {
            if (lstConns.Contains(newConn))
                throw new ApplicationException();
            else
            {
                hsUsedNodes.Add(newConn.Item1);
                NodesCount++;
                hsUsedNodes.Add(newConn.Item2);
                NodesCount++;
                lstConns.Add(newConn);
            }
        }

        internal bool HsUsedNodesContains(int index)
        {
            return hsUsedNodes.Contains(index);
        }

        internal string GetNormalizedStr()
        {
            var conns = HsConns();
            var conns2 = conns.Select(c => new Tuple<int, int>(Math.Min(c.Item1, c.Item2), Math.Max(c.Item1, c.Item2)));
            var conn3 = conns2.OrderBy(c => c.Item1);
            var str = string.Join("", conn3.Select(t => $"[{t.Item1}-{t.Item2}]"));
            return str;
        }

        /// <summary>
        /// Nodes ar valid if they are not used
        /// </summary>
        /// <param name="index1"></param>
        /// <param name="index2"></param>
        /// <returns></returns>
        internal bool IsValidNodes(int index1, int index2)
        {
            if (hsUsedNodes.Contains(index1)) return false;
            if (hsUsedNodes.Contains(index2)) return false;
            return true;

        }
    }

}
