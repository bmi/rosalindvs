﻿using RosalindVS;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Documents;

namespace CatalanWPF
{
    internal class Cat2
    {
        private string input;
        private NodeLists nlst;

        public Cat2(string input)
        {
            //input = "UAGCGUGAUCAC";
            ////       012345678901
            this.input = input;
            this.nlst = new RosalindVS.NodeLists(input);

            //var dctAU = this.find('A', 'U');
            //var dctUA = this.find('U', 'A');
            var dctC = this.find('C', 'G');
            var dctG = this.find('G', 'C');

            foreach (var kvC in dctC)
            {
                foreach (var vG in kvC.Value)
                {

                }
            }

        }

        private Dictionary<int, List<int>> find(char c1, char c2)
        {
            var dct = new Dictionary<int, List<int>>();
            for (int i1 = 0; i1 < nlst.NodesCount; i1++)
            {
                if (nlst.ListNodes[i1].Char != c1)
                    continue;
                for (int i2 = 0; i2 < nlst.NodesCount; i2++)
                {
                    if (i1 == i2) continue;
                    if (nlst.ListNodes[i2].Char != c2)
                        continue;
                    if ((i1 - i2) % 2 == 0) continue; // vzdialenost musi byt neparna
                    if (Math.Abs(i1 - i2) > 1)
                    {
                        var stn = nlst.DctNodes[i1].Next;
                        int cntA = 0;
                        int cntU = 0;
                        int cntC = 0;
                        int cntG = 0;
                        while (true)
                        {
                            switch (stn.Char)
                            {
                                case 'A': cntA++; break;
                                case 'U': cntU++; break;
                                case 'C': cntC++; break;
                                case 'G': cntG++; break;
                                default:
                                    throw new NotImplementedException();
                            }
                            if (stn.Next.Index == i2)
                                break;
                            stn = stn.Next;
                        }
                        if (cntA == cntU && cntC == cntG)
                            if (dct.ContainsKey(i1))
                                dct[i1].Add(i2);
                            else
                                dct.Add(i1, new List<int> { i2 });


                    }
                    else
                    {
                        if (dct.ContainsKey(i1))
                            dct[i1].Add(i2);
                        else
                            dct.Add(i1, new List<int> { i2 });
                    }


                }

            }
            return dct;
        }

        private bool match(Node nnext, Node nprev)
        {
            if (nnext.Char == 'A' && nprev.Char == 'U') return true;
            if (nnext.Char == 'U' && nprev.Char == 'A') return true;
            if (nnext.Char == 'C' && nprev.Char == 'G') return true;
            if (nnext.Char == 'G' && nprev.Char == 'C') return true;

            return false;
        }
    }

    //[DebuggerDisplay("{I1}-{I2}: {C1.ToString()}-{C2.ToString()}, {Valid}")]
    //public class ValidConn
    //{
    //    public int I1 { get; set; }
    //    public int I2 { get; set; }
    //    public bool Valid { get; set; }

    //    public char C1 { get; set; }
    //    public char C2 { get; set; }

    //    public ValidConn(int i1, int i2, char c1, char c2)
    //    {
    //        I1 = i1;
    //        I2 = i2;
    //        C1 = c1;
    //        C2 = c2;
    //        Valid = true;
    //    }
    //}

}