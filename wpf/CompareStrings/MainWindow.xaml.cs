﻿using RosalindVS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;

namespace CompareStrings
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //string str1 = "GTCTTTAGGAGCACATTATTACTAACTTCAGCGGCCCTAAATTTGGAACCGTCGCTATCTCTTACGGAAGTCGTGGCAGTTAACAAAAGTATGCGTTCACGCGGAGACTTGACTCGG";
        //string str2 = "CAAGTTAAAACTATGTGATCGCTGTGTGCTTGAAAAGGACAGTGCTGGGATTGTACATGTCTGGCGATAATGAGACTCTGTACCCATATCTGAGATACGACAAGTACGCGGAGATCCAGG";

        // full
        //string str1 = "TTGATACGGACAGTAAATTGGGGCAGGGAGGGTAGGATACGGTTGGGCACGGATTGTCACCTAACTCGGCATGACTAAGGCTGAGCCATGATAACGCTACTGACAACGAGACATAACCTGCGCCCCGAACTCGAATCGAACATGCGTTTTTATCACCGCGCCAGCTGGGATTTCCATTCAGATTATGGTTTCGAAAGGGGTTACCAACGCGACTTCCAAACTTCGTCCATATCGTCGCCTGTGGATGGGAAACGGACCCGTCGGAGGCCTTTACGCTCCTAGAGCTTCGAGCAGTCCCTCCGTAGCGCAGACGTCAACAGTTTACTGAGGCCGCGTGTAATGGTCTCGCGGCGCGATCTAAATCGTTAGTCATAGGTGCACCGTGACTGTGTGACAGGCATCCGAAAGTGTTTGCGGCGCTACGTGCCGTTTTATCACGTGTGGTATCAGGGATGATTTTCCCTCGTTGCGAGAACAAGAAACCCGAGCTCTCTCAGCGCTCATTGTGTGCGAGCTGCCATACGTGTTCCGCGCGCCTCCCAGATAGGTAGCTGCTTGGTTTCAAGAGCCCACCACAGTCTATGTATTCGCGGGTTCGAACTCACGTGGTCAGGCAGCGATATATACGCGCCGCCTTATCTCGACTCTTGAGCAGTTTTCTTGGGCATTTTGCCCCTCGAGCCCTGTCGCTCCCCGCTAATCGAACCGGTGATAGACGAATAAACTAACAACCAGCAAAAAATTCTACGTAACTCTCCTTATAAGCGTAGAAGAGGAGGTAGTCGGGGATGGCTGACTTTAAATCGCTGAGAGCTGAC";
        //string str2 = "CGTTCGGTTACAAACTGTCGTCGGATGAGCTCCCCTGGTTGAGCTCCATAAGCCGCCTACTGAAGTAAAGGGACGAGCATCTATGATTTATGGATAAGACCCTTGTATGTCGTCAACAAAAGCATGGGAGACGTAACAAAGGTTGTCCGACAAGAGTTACACACAGACCTATTCCTGGAAAAAACCGAGAATTTTCGAGTGTGTAGCAATCCGTATGAAATACAGGGCTTTGAGCCTGCCAATAAAGTCCGAGCTGTCGTTTTCTCCAGAGACGTCCGAAAGTTCGTGGCGCGACCGGATCTCACTTAAAACGCCTCAATGCAGGGCAACTGCGCCTAATCTAGCCACCAGCGCACCTACCACTGTGACTACGTACTCGTGGAGCTCGGACGCCTGTATTAGGCAGATACATATTTCCATTACGATTCTCGAAGGGGCGATTCTTGTCATCGTTATGTGGCACCGTGACCGTCAGCAATGGGTGGGGGAGGTGTCAGCACGGGGTAGACAAGGACCGTAACACAGCTAGACCTCTCATACTAATCTCTTGATGGTCAGGAGGCCGGAGTGTGAGCATTCACAACCGCACCAATTATATTATTCTGCCACGGACATAAGAGACTCGAACAACCCAGAGAGCACATGCGATTCTGTCACGCAGACACAATCTGCGCCCTGTCGAGTTTATGGCTGAGGAGCATTTCACGCTTCGCGACTTCCCCGTTATCTTAACGGGATTTTATTCATGGTTACCTCCTAAAGGGTCGAGGTACACTAATATCATATAGTGGTTTGTGACTACATCGGTTGATTTCCTGAAAGGGAAGTGATGAACTAGCGCCTTAGGCTTGTGTAGTAAAGCAAACCCCTTACGCGAAGGAGGCGTGGGAGCATCCCAACCTGACA";
        string str1 = "AACCTTGG";
        string str2 = "ACACTGTGA";

        //string str1 = "GCACATTATTAC";
        //string str2 = "CTATGTGATCGCTGTGTGCT";
        int counter;

        string best = "";
        int max1idx, max2idx;
        Dictionary<Tuple<char, int>, IEnumerable<int>> dctGetAllOccurencesByIndex;
        Dictionary<string, int> dct;
        Dictionary<Tuple<int, int>, string> dctFind;

        public MainWindow()
        {
            InitializeComponent();
            //var a = Tools.LoadFastaInput(@"c:\Downloads\rosalind_lcsq.txt");

            var s1 = string.Join(";", str1.ToCharArray());
            var s2 = string.Join(";", str2.ToCharArray());

            dctGetAllOccurencesByIndex = new Dictionary<Tuple<char, int>, IEnumerable<int>>();
            dctFind = new Dictionary<Tuple<int, int>, string>();
            dct = new Dictionary<string, int>();
            int i2;

            max1idx = str1.Length - 1; // ak bude i1 > ako max1idx - okamzity return
            max2idx = str2.Length - 1;
            for (int i1 = 0; i1 < str1.Length - 1; i1++)
            {
                Debug.WriteLine(i1);
                var key = new Tuple<int, int>(i1, 0);
                find(key, "", 0);
            }
            Debug.WriteLine($"LONGEST: {best.Length} {best}");
        }
        // davat vysledky do dictionary inak ne je sanca

        private string find(Tuple<int, int> key, string fromPrev, int deep)
        {
            counter++;

            if (dctFind.ContainsKey(key))
                return dctFind[key];
            else
                dctFind.Add(key, "");

            var occrs2 = getAllOccurencesOf(key.Item1, key.Item2);
            if (!occrs2.Any())
            {
                return "";
            }

            dctFind[key] += str1[key.Item1];
            if (dctFind[key].Length > best.Length)
            {
                best = dctFind[key];
                Debug.WriteLine(best);
            }
            else if (dctFind[key].Length == best.Length)
            {
                Debug.WriteLine($"{dctFind[key].Length}, {key.Item1}, {key.Item2}, {dctFind[key]}");
            }
            // pokus najst dalsiu dvojicu
            foreach (var i2n in occrs2)
            {
                if (i2n < max2idx) // nema vyznam hladat za koncom str2
                {
                    for (int i1n = key.Item1 + 1; i1n <= max1idx; i1n++)
                    {
                        var nk = new Tuple<int, int>(i1n, i2n + 1);
                        var snk = find(nk, fromPrev, deep + 1);
                        if (!dctFind.ContainsKey(nk))
                            dctFind.Add(nk, "");
                        dctFind[nk] += dctFind[key] + find(nk, fromPrev, deep + 1); // tu sa hladaju dalsie znak(y)
                    }
                }
            }
            return dctFind[key];
        }


        private IEnumerable<int> getAllOccurencesOf(int i1, int fromIdx2)
        {
            char c = str1[i1];
            var key = new Tuple<char, int>(c, fromIdx2);
            if (dctGetAllOccurencesByIndex.ContainsKey(key))
                return dctGetAllOccurencesByIndex[key];
            var arr = Tools.OccurencesList(str2, c, fromIdx2);
            dctGetAllOccurencesByIndex.Add(key, arr);
            return arr;
        }
    }

    public class CData
    {
        public int Index { get; set; }
        public char C { get; set; }


    }
}
