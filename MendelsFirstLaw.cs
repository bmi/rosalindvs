﻿using System.Collections.Generic;
using System.Linq;

namespace RosalindVS
{
    internal class MendelsFirstLaw
    {
        public MendelsFirstLaw()
        {
            /*
            k individuals are homozygous dominant for a factor YY
            m are heterozygous Yy
            n are homozygous recessive. yy
            */
            var k = 25; // YY 
            var m = 26; // Yy 
            var n = 22; // yy 
            var lst = new List<string>();
            var total = 0;
            var totalY = 0;

            // set parents
            for (int i = 0; i < k; i++) lst.Add("YY");
            for (int i = 0; i < m; i++) lst.Add("Yy");
            for (int i = 0; i < n; i++) lst.Add("yy");

            // breeding
            for (int i = 0; i < lst.Count; i++)
            {
                for (int j = i + 1; j < lst.Count; j++)
                {
                    var a = lst[i];
                    var b = lst[j];
                    if (a[0] == 'Y' || b[0] == 'Y') totalY++;
                    if (a[0] == 'Y' || b[1] == 'Y') totalY++;
                    if (a[1] == 'Y' || b[0] == 'Y') totalY++;
                    if (a[1] == 'Y' || b[1] == 'Y') totalY++;
                    total += 4;
                }
            }
            var resultProbabilityY = System.Math.Round((double)totalY / (double)total, 5); // result here
        }
    }
}