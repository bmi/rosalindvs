﻿using Microsoft.CSharp;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RosalindVS
{
    internal class EnumeratingGeneOrders
    {
        public EnumeratingGeneOrders(int max)
        {
            var results = new StringBuilder();
            var result = GetPermutations(Enumerable.Range(1, max), max);
            results.AppendLine($"{result.Count()}");
            foreach (var item in result)
                results.AppendLine(string.Join(" ", item));
            var r = results.ToString();

            IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
            {
                if (length == 1) return list.Select(t => new T[] { t });
                return GetPermutations(list, length - 1)
                    .SelectMany(t => list.Where(e => !t.Contains(e)),
                        (t1, t2) => t1.Concat(new T[] { t2 }));
            }
        }

    }

}