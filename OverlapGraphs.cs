﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class OverlapGraphs
    {

        public OverlapGraphs(int overlap)
        {
            var input = @">Rosalind_0498
AAATAAA
>Rosalind_2391
AAATTTT
>Rosalind_2323
TTTTCCC
>Rosalind_0442
AAATCCC
>Rosalind_5013
GGGTGGG";

            var data = getFASTAData(input).ToArray();
            var sbResults = new StringBuilder();
            for (int i = 0; i < data.Length - 1; i++)
            {
                var s = data[i];
                var sPrefix = s.Item2.Substring(0, overlap);
                var sPostfix = s.Item2.Substring(s.Item2.Length - overlap);
                for (int j = i + 1; j < data.Length; j++)
                {
                    var t = data[j];
                    var tPrefix = t.Item2.Substring(0, overlap);
                    var tPostfix = t.Item2.Substring(t.Item2.Length - overlap);
                    if (sPostfix == tPrefix)
                    {
                        if( !s.Item2.Equals(t.Item2))
                            sbResults.AppendLine($"{s.Item1} {t.Item1}");
                    }
                    else if (sPrefix == tPostfix)
                    {
                        if( !s.Item2.Equals(t.Item2))
                            sbResults.AppendLine($"{t.Item1} {s.Item1}");
                    }
                }
            }

            var result = sbResults.ToString();              // result is here

            // getting collection from string, Item1=Name. Item2=data
            IEnumerable<Tuple<string, string>> getFASTAData(string strFASTA)
            {
                return strFASTA
                    .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                    .Select(strArr => new Tuple<string, string>(strArr[0].Trim(), string.Join("", strArr.Skip(1)).Trim()));
            }

        }
    }
}