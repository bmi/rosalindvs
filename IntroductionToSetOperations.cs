﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RosalindVS
{
    internal class IntroductionToSetOperations
    {
        public IntroductionToSetOperations()
        {
            var inputLines = File.ReadAllLines(@"h:\Downloads\rosalind_seto.txt");
            var n = int.Parse(inputLines[0]);
            var divs = new char[] { '{', '}', ',', ' ' };
            var A = inputLines[1].Split(divs, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToHashSet();
            var B = inputLines[2].Split(divs, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToHashSet();
            var U = Enumerable.Range(1, n);
            var AuB = new HashSet<int>(A); // union
            var AiB = new HashSet<int>(A); // intersection
            AuB.UnionWith(B);
            AiB.IntersectWith(B);

            // result here
            var result = $@"{{{string.Join(", ", AuB)}}}
{{{string.Join(", ", AiB)}}}
{{{string.Join(", ", A.Where(x => !B.Contains(x)))}}}
{{{string.Join(", ", B.Where(x => !A.Contains(x)))}}}
{{{string.Join(", ", U.Where(x => !A.Contains(x)))}}}
{{{string.Join(", ", U.Where(x => !B.Contains(x)))}}}";

        }
    }
}