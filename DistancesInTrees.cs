﻿using System;
using System.Linq;

namespace RosalindVS
{
    internal class DistancesInTrees
    {
        public DistancesInTrees()
        {
            string result = "";

            var lines = System.IO.File.ReadAllText(@"h:\Downloads\rosalind_nwck.txt")
                .Split(new string[] { System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
            var count = lines.Length >> 1;
            var data = Enumerable.Range(0, count)
                .Select(idx => new
                {
                    tree = lines[idx * 2],
                    node1 = (lines[idx * 2 + 1].Split(' ')[0]),
                    node2 = (lines[idx * 2 + 1].Split(' ')[1])
                });

            Console.WriteLine(treeReduction("dog", "cat", "(cat)dog;"));
            Console.WriteLine(treeReduction("dog", "cat", "(dog,cat);"));

            foreach (var item in data)
            {
                result = treeReduction(item.node1, item.node2, item.tree);
                Console.WriteLine($"{item.node1}, {result}");
            }
            Console.ReadKey();

            result = treeReduction("dog", "cat", "(dog,cat);");


            result = treeReduction("Certhia_rufina", "Euspiza_hendricksoni", "(Almo_geyri,Apalone_kuhli,Bradypterus_hypomelus,Branta_barbatus,Buthacus_strepera,Calidris_mehelyi,Candoia_stimsoni,Casarca_lutra,Certhia_rufina,Chen_falcinellus,Chettussia_cyanea,Chrysemys_casualis,Chrysopelea_caeruleus,Circus_alpinus,Colaeus_chinensis,Coleonyx_cristatella,Corvus_hodgsoni,Crocodylus_auriculatus,Cuculus_himalayensis,Cuora_caudatus,Damon_versicolor,Dasypeltis_erythronota,Dendrelaphis_cornix,Epipedobates_fuellebornii,Eschrichtius_cranwelli,Eucratoscelus_rubida,Eucratoscelus_tetrix,Euspiza_bukhunensis,Euspiza_hendricksoni,Fulica_tigris,Gypaetus_deserti,Haliaetus_bewickii,Haliaetus_borealis,Holaspis_pygmeus,Hyperoodon_armata,Hyperoodon_leiosoma,Ketupa_cianeus,Lagenorhynchus_troglodytes,Latastia_ferruginea,Leiocephalus_decorus,Leiopython_elegans,Leptobrachium_plumipes,Leptopelis_sp,Limosa_resinifictrix,Lystrophis_ibis,Marmota_rostratus,Marmota_subcinctus,Megaloperdix_Anas,Micropalama_zonata,Monticola_sinensis,Motacilla_licin,Nhandu_alcinous,Ninox_fluviatilis,Nucifraga_porphyrio,Oceanodroma_musculus,Odonthurus_livia,Oedura_dentatus,Oligodon_atra,Osteopilus_ferruginea,Otis_colubrinus,Otocoris_aestivus,Otocoris_oedicnemus,Pagophila_caerulea,Parabuthus_leucoryphus,Pelecanus_hendricksoni,Pelodytes_cygnus,Phelsuma_mystacinus,Phormictopus_indicus,Phrynops_colchicus,Phrynosoma_margaritifera,Phrynosoma_stellatum,Platemys_albirostris,Pleurodeles_cambridgei,Procellaria_grunniens,Regulus_eremita,Rhynchaspis_plumipes,Rissa_prominanus,Rufibrenta_undulata,Salamandra_caudatus,Scaphiophryne_tetrax,Scaphiopus_wislizeni,Sceloporus_boyciana,Sitta_borealis,Spalerosophis_cristatella,Spermophilus_keyzerlingii,Sterna_mykiss,Streptopelia_means,Sturnus_dauricus,Tadarida_irregularis,Telescopus_melanuroides,Thymallus_japonica,Tiliqua_turtor,Triturus_reinwardti,Uncia_bonasus,Underwoodisaurus_tetrix,Upupa_hypomelus,Zosterops_unicolor)Acheron_rostratus;");

            result = treeReduction("Anthropoidae_nebularia", "Apodora_zonata", "((((((((((((((((((((((((,((((((,((,(,)),)),(,)),(((,),),)),),),(,((,),)))),),),),((,),)),),(,)),((((((((((Anthropoidae_nebularia,),),),),),),((,),)),),),)),),(,)),(,)),(,)),),),(,)),),),),((,),)),),(((,((,),(,(,)))),),((((((((,),),),),),),((,),)),((,),)))),),Apodora_zonata),((,((,),)),),((((,((,),(,(,)))),),),));");



        }

        private string treeReduction(string node1, string node2, string input)
        {
            var str = input
                .Replace(node1, "#")
                .Replace(node2, "$")
                .ToUpper();

            for (char i = 'A'; i <= 'Z'; i++)
                while (str.Contains(i.ToString())) str = str.Replace(i.ToString(), "");
            str = str
                .Replace("_", "")
                .Replace(";", "");


            // remove ,,
            while (str.Contains(",,")) str = str.Replace(",,", ",");

            // remove (,)
            while (str.Contains("(,)")) str = str.Replace("(,)", "");

            // remove ,)
            while (str.Contains(",)")) str = str.Replace(",)", ")");

            // remove ()
            while (str.Contains("()")) str = str.Replace("()", "");


            //// remove (,
            //while (str.Contains("(,")) str = str.Replace("(,", ")");

            var idxL = str.IndexOf('#');
            var idxH = str.IndexOf('$');
            if( idxL > idxH)
            {
                var a = idxL;
                idxL = idxH;
                idxH = a;
            }
            var data = str.Substring(idxL + 1, idxH - idxL - 1);
            var occL = Tools.Occurences(data, "(");
            var occR = Tools.Occurences(data, ")");

            return $"{str} - L:{occL}, R: {occR} = {occL + occR + 1}";


        }
    }
}