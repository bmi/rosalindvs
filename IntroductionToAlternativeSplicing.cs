﻿using System.Collections.Generic;
using System.Numerics;

namespace RosalindVS
{
    internal class IntroductionToAlternativeSplicing
    {
        public IntroductionToAlternativeSplicing()
        {
            var n = 6;
            var m = 3;
            BigInteger sum = 1; // none combination
            for (int k = m; k < n; k++)
                sum += CombinationsCount(n, k);
            var result = sum % 1000000; // result here
        }

        public static BigInteger CombinationsCount(int n, int r)
        {
            return Factorial(n) / (Factorial(r) * Factorial(n - r));
        }

        static Dictionary<int, BigInteger> dctFactorial = new Dictionary<int, BigInteger>(); // cache for factorial
        public static BigInteger Factorial(int i)
        {
            if (i <= 1)
                return 1;
            if (!dctFactorial.ContainsKey(i))
                dctFactorial.Add(i, i * Factorial(i - 1));
            return dctFactorial[i];
        }
    }
}