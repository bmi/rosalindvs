﻿using System;

namespace RosalindVS
{
    internal class RabbitsAndRecurrenceRelations
    {
        public RabbitsAndRecurrenceRelations(int maxDepth, ulong breedPairsCount)
        {
            ulong youngRabbitsCount = 1;
            ulong total = 1;
            ulong adultRabbitsCount = 0;
            for (int epoch = 0; epoch < maxDepth - 1; epoch++)
            {
                var newRabitsCount = adultRabbitsCount * breedPairsCount;
                total += newRabitsCount;
                adultRabbitsCount += youngRabbitsCount;
                youngRabbitsCount = newRabitsCount;
            }
            Console.WriteLine($"Total: {total}");
        }
    }
}