﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosalindVS
{
    public class NodeLists
    {

        public List<Node> ListNodes { get; private set; }
        public Dictionary<int, Node> DctNodes { get; private set; }
        public Dictionary<char, char> DctCoChar { get; private set; }
        public int NodesCount { get; private set; }
        public int HalfNodesCount { get; }

        public NodeLists(string input)
        {
            ListNodes = new List<Node>();
            DctNodes = new Dictionary<int, Node>();

            int idx = 0;
            Node prevNode = null;
            foreach (var c in input)
            {
                var node = new Node(c, prevNode, idx++);
                ListNodes.Add(node);
                DctNodes.Add(node.Index, node);
                if (prevNode != null)
                    prevNode.Next = node;
                prevNode = node;
            }
            // make toroidal
            ListNodes[0].Prev = ListNodes.Last();
            ListNodes.Last().Next = ListNodes[0];
            NodesCount = ListNodes.Count;
            HalfNodesCount = NodesCount >> 1;
            DctCoChar = new Dictionary<char, char>();
            DctCoChar.Add('A', 'U');
            DctCoChar.Add('U', 'A');
            DctCoChar.Add('C', 'G');
            DctCoChar.Add('G', 'C');

            FindAllValidConnections();
        }


        public void FindAllValidConnections()
        {
            foreach (var node in ListNodes)
            {

                findValidConnections(node);

            }
        }


        public Node[] GetNodesWithXConn(int x)
        {
            return ListNodes.Where(n => n.ValidConnections.Count == x).ToArray();
        }





        private void findValidConnections(Node node)
        {
            var dct = new Dictionary<char, int>();
            dct.Add('A', 0);
            dct.Add('U', 0);
            dct.Add('C', 0);
            dct.Add('G', 0);
            dct[node.Char]++;
            char coChar = ' ';
            var n2 = node;
            coChar = DctCoChar[node.Char];
            node.ValidConnections.Clear();
            int dist = 0;
            while (true)
            {
                n2 = n2.Next;
                if (n2.Index == node.Index)
                    break;
                dist++;
                dct[n2.Char]++;

                var isOk = dct['A'] == dct['U'] && dct['C'] == dct['G'] && n2.Char == coChar;
                if (isOk)
                {
                    node.ValidConnections.Add(n2);
                }

            }
        }


    }



    [DebuggerDisplay("{Index}: {Char}, #{ValidConnections.Count}")]
    public class Node
    {
        public char Char { get; }
        public Node Prev { get; set; }
        public Node Next { get; set; }
        public int Index { get; }
        public char NodeType { get; private set; }

        public List<Node> ValidConnections { get; private set; }
        public double X { get; set; }
        public double Y { get; set; }


        public Node(char c, Node prevNode, int index)
        {
            this.Char = c;
            this.NodeType = "AU".Contains(c) ? 'A' : 'C';
            this.Prev = prevNode;
            this.Index = index;
            ValidConnections = new List<Node>();
        }

    }




}
