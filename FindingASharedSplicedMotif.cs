﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Randomizations;
using GeneticSharp.Domain.Reinsertions;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace RosalindVS
{
    internal class FindingASharedSplicedMotif
    {
        public FindingASharedSplicedMotif()
        {
            var fasta = LoadFastaInput(@"h:\Downloads\rosalind_lcsq.txt");

            // fill map
            var X = fasta.First().Value.ToCharArray();
            var Y = fasta.Last().Value.ToCharArray();
            var map = new int[X.Length + 1, Y.Length + 1];
            for (int i = 1; i <= X.Length; i++)
            {
                for (int j = 1; j <= Y.Length; j++)
                {
                    if (X[i - 1] == Y[j - 1])
                        map[i, j] = map[i - 1, j - 1] + 1;
                    else
                        map[i, j] = Math.Max(map[i, j - 1], map[i - 1, j]);
                }
            }

            // get one of solution
            var sb = new StringBuilder();
            var x = X.Length; // start from end
            var y = Y.Length;
            while (true)
            {
                var cv = map[x, y];                 // current value
                while (map[x, y - 1] == cv) y--;    // go backward and find first value
                while (map[x - 1, y] == cv) x--;
                sb.Insert(0, X[x - 1]);             // store char in reverse order
                x--; y--;
                if (x == 0 || y == 0)               // stop on begin
                    break;
            }

            var result = sb.ToString(); // result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }
    }
}