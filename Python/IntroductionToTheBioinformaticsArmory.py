from Bio.Seq import Seq
my_seq = Seq("TGGACCACCGGACCGGAGGTTTGTAACGCGGTCCCATAACATTCGTAAATTCCTACCCTCTTATCAAACTGATTCACAGATAGCCTGACGATAGCTCGCACGAAAGACCATCCCGTTAGCCACGCGTCAGTGATTTACTTGCTCTCCTAAAGGGAGCCGAGTTTGGAGCTATAACATAGGCCCAGGCGGCTTGCGCTACCTACAGCACCTTAGAGCCTCTGTAGAGGCTTCGACCATTAAGCCCGAACCCAGACGACGCGCACGGGCGCCTTAGGATTTTCGGTGCGGGAAATAGTGATGCGCCTTTTGGCATCCTGAATGATGCCGCGGCCACCGCTTAACCTTTGACGGGGTGAGGTGTAGAAGATTTAGTGGAATAATAAGGCTTCACGGCATAGAAGGTCTTGCCTTTTATTTGCCCGTAGTGGGGAACAAACGTGCCCACGATTATTCTGATACCTCCGCAATGTCACTAAAAATTAAAGCAGTTCGGTTGGCATGAACACGCTCCCAACGGGGATGACACTCCTAGGAGTCCGGAAAACTGGCCTGGAAGACGGTCTGCTAGCGTGGGATCCAAGGTCCGCTTGTATTTCTAATGCATATTTCGCTTCGCTCTCGCCACAGGAGTCTTGACTCTAAATCGTAAATCCGTTGCTAAGGCCTAAATAGAGATCTTCTCAAGATGGTAACGCTAGCCACAGATTTGTTCGGGCACTAAAGATCCGAGCTATCACCGCGAAAACCCCGTGAGTCTTCAAGGTTACCATGTAAGATGAAAACCGTGCAAGAAAGGATCCAGGATTCTGATGCAGCCTCGGGCGTCACGGCGTCGGGTATACAGGTCCGTATGTACCGCTTACGCCAGGGCTGGTTTAGTTTTAACTACTTGGTGTGCTAAGCACTCACCCCCTTCCCGACAGTCCACTGCGAATCGCAGTCCAAGAGTTAGGTGTAGACCATCACC")

ca = my_seq.count("A")
cg = my_seq.count("G")
ct = my_seq.count("T")
cc = my_seq.__len__() - ca - cg - ct
print(ca, cc, cg, ct )