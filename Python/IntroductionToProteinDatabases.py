#from Bio import SeqIO
import urllib.request
for line in urllib.request.urlopen("http://www.uniprot.org/uniprot/B5ZC00.txt"): # Q5SLP9, B5ZC00, 
    sLine = str(line)
    if sLine.startswith("b'DR") and sLine.__contains__('GO; GO:') and sLine.__contains__('; P:'):
        pIdx = sLine.index('; P:') + 4
        pIdx2 = sLine.index(';', pIdx)
        print(sLine[pIdx:pIdx2])
