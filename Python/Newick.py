from Bio import Phylo
from io import StringIO



#title = "Lamprolepis_monachus Rhynchaspis_griseus"
#data = "(((((((((((((((((((((,),),(,(,))),(,((((((((,),),),),),),),))),),(,)),(,)),),),(,)),),(,)),),(,)),(((((((((((((((((,(,)),),),((,),)),),((,),)),),((,),)),),Lamprolepis_monachus),),((,),)),),(,)),),),Rhynchaspis_griseus)),),),(,)),(((((,),),),),((((,),),(,(,))),(,)))),),,((((((((,),((,),)),((,),)),),(((,),),)),),),));"

#title = "Prunella_walti Dipus_miliaris"
#data = "((((((((((,),),Prunella_walti),),),),),),),(((((((((((((((((,),(,)),),),),(,((,),))),),(,(,))),((,),)),((((((((((,((,(,(,))),)),),),(((((,),),((((,((,),)),(Dipus_miliaris,((((,),),),(,(,))))),(,)),)),),)),(,)),),),),),)),((,),)),(,)),((,),)),((,),)),),),(,(((,),(,(,))),))),(,((,),)));"

# title = "s(1)"
# data = "(cat)dog;"

title = "s(2)"
data = "(dog,cat);"


title = "Limosa_agama Gonyosoma_alcinous"
data = "((((((((((((((((Acanthosaura_fusca,Teratoscincus_nigriceps),((Bos_carnifex,Chelodina_bengalensis),(Mochlus_drapiezii,Vulpanser_cachinans))),(Lepus_fusca,(Limnaeus_pholeter,(Net_cocincinus,Pleurodeles_equestris)))),Pseudorca_fulvus),(((Anodonta_pallasii,Opheodrys_gordoni),(Cyclagras_fernandi,((Eudrornias_haliaetus,Homalopsis_ferox),((Natriciteres_murinus,((Nhandu_resinifictrix,(Strepsilas_molurus,Xenochrophis_breitensteini)),Petrocincla_glacialis)),Passer_leucomystax)))),(Apus_variegatus,(Coturnix_semipalmatus,Otis_politus)))),Xenophrys_angustirostris),Nhandu_pallidus),(Bombina_fulvus,((((Elseya_mugodjaricus,((Ingerophrynus_colubrinus,Petrocincla_dauricus),Porzana_lividum)),((Gongylophis_grus,Salmo_novaeangliae),Thamnophis_citrsola)),Odobenus_grupus),Numenius_siebenrocki))),Sturnus_hirundo),Triturus_zenobia),Rhabdophis_bonasus),Ziphius_hirundo),(Dafila_apus,((((((Haplopelma_brongersmai,Vulpes_collybitus),Tetraogallus_clypeata),Numenius_difficilis),Hydrochelidon_mehelyi),Trachemys_africanus),Sternotherus_tarandus))),Fuligula_sepsoides),(Capra_dahurica,(((Gonyosoma_alcinous,(Minipterus_aleutica,Tursiops_lagopus)),Thymallus_nupta),Sitta_atrigularis))),(((Allobates_porphyrio,Bradypterus_nigra),Saxicola_crispus),(Ardea_decorus,Dendrobates_calyptratus)),((((((Amphiuma_caninus,(((Eutamias_rusticolus,Hirundo_heudei),(((Holaspis_fernandi,Megaptera_floridana),Nipponia_hilarii),Phormictopus_ceterus)),Kinosternon_apus)),Anser_avicularia),Neophron_carbonaria),((((((((((((Bubulcus_doriae,Elaphe_cygnoides),Buthus_leschenaultii),(Cyclemys_monacha,Nyctaalus_colubrinus)),Cardiocranius_capra),Capella_guttata),Osteopilus_ignicapillus),Scaphiophryne_sibiricus),(Chrttusia_barbata,Hadogenes_battersbyi)),Ctenosaura_terrestris),Eirenis_novaeguineae),Lycodon_czerskii),(Limosa_agama,Perdix_circia))),(Aythia_alterna,Philomachus_milii)),Cottus_deminutus));"


tree = Phylo.read(StringIO(data), "newick")
print(title)




tree.ladderize()

dist = tree.trace("Limosa_agama", "Gonyosoma_alcinous")
for clade in dist:
    print(clade)
print('dist', dist)
Phylo.draw(tree)