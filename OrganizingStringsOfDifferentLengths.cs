﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;

namespace RosalindVS
{
    internal class OrganizingStringsOfDifferentLengths
    {
        public OrganizingStringsOfDifferentLengths()
        {
            var input = "Y R A T Q N K G U V P L";
            int max = 3;

            var cmpInput = " " + input.Replace(" ", "");
            var arr = input.Split(' ');

            var lst = new List<string>();
            for (int i = 1; i <= max; i++)
            {
                var perms = GetPermutations(arr, i)
                    .Select(r => string.Join("", r.ToArray()))
                    .Select(s => s.PadRight(max, ' '))
                    .ToArray();
                lst.AddRange(perms);
            }
            lst.Sort((s, t) => 
            {
                for (int i = 0; i < max; i++)
                {
                    if (s[i] == t[i]) continue;
                    return cmpInput.IndexOf(s[i]) - cmpInput.IndexOf(t[i]);
                }
                return 1;
            });
            var result = string.Join(Environment.NewLine, lst.Select(r => r.TrimEnd()));
        }

        IEnumerable<IEnumerable<T>> GetPermutations<T>(IEnumerable<T> list, int length)
        {
            if (length == 1) return list.Select(t => new T[] { t });
            return GetPermutations(list, length - 1)
                .SelectMany(t => list.Where(e => true),
                    (t1, t2) => t1.Concat(new T[] { t2 }));
        }
    }
}