﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Markup;

namespace RosalindVS
{
    internal class LongestIncreasingSubsequence
    {
        public LongestIncreasingSubsequence()
        {
            var input = "5 1 4 2 3";
            var data = input
                .Split(' ')
                .Select(s => int.Parse(s)).ToList();

            var lis = getResult(data, false);
            var lds = getResult(data, true);
            var result = $"{string.Join(" ", lis)}{Environment.NewLine}{string.Join(" ", lds)}"; // result
        }

        private int[] getResult(List<int> inputList, bool negate)
        {
            var values = inputList;
            if (negate)
                values = values.Reverse<int>().ToList();
            var comparer = Comparer<int>.Default;
            var pileTops = new List<int>();
            var pileAssignments = new int[values.Count];
            for (int i = 0; i < values.Count; i++)
            {
                var element = values[i];
                int pile = pileTops.BinarySearch(element, comparer);
                if (pile < 0)
                    pile = ~pile;
                if (pile == pileTops.Count) pileTops.Add(element);
                else pileTops[pile] = element;
                pileAssignments[i] = pile;
            }
            var result = new int[pileTops.Count];
            for (int i = pileAssignments.Length - 1, p = pileTops.Count - 1; p >= 0; i--)
            {
                if (pileAssignments[i] == p)
                    result[p--] = values[i];
            }
            if (negate)
                result = result.Reverse<int>().ToArray();
            return result;
        }
    }
}