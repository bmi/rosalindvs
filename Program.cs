﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace RosalindVS
{
    class Program
    {
        static void Main(string[] args)
        {
            //var perf = Tools.TestPerf(() => Math.Max(100, 200), () => max(100, 200), 100000000);
            //var perf2 = Tools.TestPerf(() => max(100, 200), () => Math.Max(100, 200), 100000000 );
            // new CountingDNANucleotides();
            // new RabbitsAndRecurrenceRelations(28, 5);
            // new IdentifyingUnknownDNAQuickly();
            // new CountingPointMutations();
            // new TranslatingRNAIntoProtein();
            // new FindingAMotifInDNA();
            // new FindingAMostLikelyCommonAncestor();
            // new MortalFibonacciRabbits(90, 17);
            // new OverlapGraphs(3);
            // new FindingASharedMotif();
            // new FindingAProteinMotif();
            // new EnumeratingGeneOrders(7);
            // new CalculatingProteinMass();
            // new LocatingRestrictionSites();
            // new MendelsFirstLaw();
            // new EnumeratingKMersLexicographically();
            // new LongestIncreasingSubsequence();
            // new OrganizingStringsOfDifferentLengths();
            // new ReversalDistance(); GA
            // new CalculateExpectedOffspring();
            // new GenomeAssemblyAsShortestSuperstring();
            // new SortingByReversals(); GA
            // new PerfectMatchingsandRNASecondaryStructures();
            // new InferringMRNAFromProtein();
            // new PartialPermutations();
            // new EnumeratingOrientedGeneOrderings();
            // new OpenReadingFrames();
            // new RNASplicing(); 
            // new FindingASplicedMotif();
            // new TransitionsAndTransversions();
            // new CompletingATree();
            // new IntroductionToSetOperations();
            // new ErrorCorrectionInReads();
            // new CountingSubsets();
            // new CatalanNumbersAndRNASecondaryStructures(); // not solved
            // new FindingASharedSplicedMotif();
            // new CreatingADistanceMatrix();
            // new MaximumMatchingsAndRNASecondaryStructures();
            // new KMerComposition();
            // new IntroductionToAlternativeSplicing();
            // new GAEditDistance();
            new DistancesInTrees();
        }


    }
}
