﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;

namespace RosalindVS
{
    internal class LocatingRestrictionSites
    {

        Dictionary<string, bool> dctIsPalindrome = new Dictionary<string, bool>();
        StringBuilder sbResults = new StringBuilder();
        int cacheHits = 0;
        public LocatingRestrictionSites()
        {
            var source = @"GCGCCACCCTGCCGACCTCTGATTCAAAGGGCGAGAAGAAATGATCGAGGAGTATTTAGC
TTGTCAAAAGTGAACAGCCACCTAGAGAATATTCGTCATTGCGTATCGCCATTATGACAC
AGTAGTGGTAGCGATGAGCAGTGCGTCAAGTTCCCCATCCGGCTCGTAATATCCGTGGGA
CTCTCAAACCTAACCCTCTCTCCAGAAATCGTACAACGAGGCCTAGGGGAGAGACCATGC
GAGAAAATAAGCGGCCGTTCTGGGCTAGCAAACGCGGTCAGCAGCTCATATGTATCCAAA
AATCCTATTAAAATGATTAAACACCGCCAGCTATCCTTGATGCCCCGGGTTAGTAAAGAT
ACCGTTTAGAAGCTATGTCCCGATGGAATCAACACTACCCGTACAAATAGGGTAACCGCG
GTTAATATTCGAGCATGTCGTCACATGGCTGATCCGCTGAAATACTGCGATTACCGTAAG
CCCATCTTATCCCCGTCTCAAGCAATTGTGAAAGTGTACAGCGACCCGCTACTTTTATGT
CCGGTGTATGTCGCGCATGGCCAGAAAGTAATGAGGACGATCCGTCATTCCAGTCCCAAC
AACAGCGCCAGACAAACCTCATTAGCTAACCCTGGAGCCCTAGGCAGAGACGATACAGTT
TTCAGTGACCATTGGCACGCTTCTGAGCAAGGCCTAAGCTGGAATCTTCGCCTCGCGTGC
CGCACATTGGCATCAGATCGCCACACTAGAGGATCAGCGGCAAACTTAGTTTGACACTTG
ATGCACGGGTTAGCGGACTGGTATTCGGCTATATCCGGCCCGTGGGCTACCTGCGC".Replace(Environment.NewLine, "");

            var sw = new Stopwatch();
            sw.Start();
            checkForRevPalindromes(source, 4, 12);
            sw.Stop();
            var result = sbResults.ToString(); // result here
        }


        /// <summary>
        /// Recursive, checking all substring variants
        /// </summary>
        /// <param name="src"></param>
        /// <param name="len"></param>
        /// <param name="maxLen"></param>
        private void checkForRevPalindromes(string src, int len, int maxLen)
        {
            for (int pos = 0; pos <= src.Length - len; pos++)
            {
                var str = src.Substring(pos, len);
                if (isReversePalindrome(str))
                    sbResults.AppendLine($"{pos + 1} {len}");
            }
            if (len < maxLen)
                checkForRevPalindromes(src, len + 1, maxLen);
        }

        /// <summary>
        /// Cached check for reverse palindrom
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private bool isReversePalindrome(string str)
        {
            string revPal;
            bool rtn;
            if (!dctIsPalindrome.ContainsKey(str))
            {
                revPal = getRevComplement(str);
                rtn = revPal == str;
                dctIsPalindrome.Add(str, rtn);
            }
            else
            {
                rtn = dctIsPalindrome[str];
                cacheHits++;
            }
            return rtn;
        }

        /// <summary>
        /// Reverse complement
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        string getRevComplement(string src)
        {
            char[] arr = src.ToCharArray();
            Array.Reverse(arr);
            var sRevCompl = new string(arr);
            sRevCompl = sRevCompl.Replace('A', 'a').Replace('C', 'c');
            sRevCompl = sRevCompl.Replace('T', 'A').Replace('G', 'C');
            sRevCompl = sRevCompl.Replace('a', 'T').Replace('c', 'G');
            return sRevCompl;
        }
    }
}