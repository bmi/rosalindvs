﻿using GeneticSharp.Domain.Crossovers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace RosalindVS
{
    internal class PerfectMatchingsandRNASecondaryStructures
    {
        public PerfectMatchingsandRNASecondaryStructures()
        {
            var input = LoadFastaInput("input.txt").First();
            var result = Factorial((BigInteger)input.Value.Count(c => c == 'A')) 
                       * Factorial((BigInteger)input.Value.Count(c => c == 'C'));
        }

        public BigInteger Factorial(BigInteger f)
        {
            if (f == 0)
                return 1;
            else
                return f * Factorial(f - 1);
        }

        /// <summary>
        /// Load fasta data
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

    }
}