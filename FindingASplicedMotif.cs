﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class FindingASplicedMotif
    {
        StringBuilder sbResult = new StringBuilder();
        public FindingASplicedMotif()
        {
            var data = LoadFastaInput(@"h:\Downloads\rosalind_sseq.txt");
            var sourceStr = data.First().Value;
            var motifChars = data.Last().Value;
            findIndexes(sourceStr, motifChars, 0, 0); // recursive search
            var result = sbResult.ToString(); // result here
        }

        private int findIndexes(string sourceStr, string motifChars, int sourceStrIdx, int motifCharsIdx)
        {
            if (motifCharsIdx >= motifChars.Length)
                return -1;
            sourceStrIdx = sourceStr.IndexOf(motifChars[motifCharsIdx++], sourceStrIdx);
            sbResult.Append($"{++sourceStrIdx} ");
            return findIndexes(sourceStr, motifChars, sourceStrIdx, motifCharsIdx);
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }
    }
}