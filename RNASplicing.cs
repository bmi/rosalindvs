﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RosalindVS
{
    internal class RNASplicing
    {
        public RNASplicing()
        {
            var fasta = LoadFastaInput(@"c:\Downloads\rosalind_splc.txt");  // load fasta data
            var inputDNA = fasta.First().Value;
            var intronsDNA = fasta.Skip(1);

            intronsDNA
                .ToList()
                .ForEach(intr => inputDNA = inputDNA.Replace(intr.Value, ""));   // remove introns
            var rna = DNA2RNA(inputDNA);                      // translate to RNA
            var dctTran = GetRNACodonTable();                 // get translation table     
            var c3 = Split(rna, 3).ToArray();                 // divide to 3 chars array
            var trn = string.Join("", c3.Select(c => dctTran[c]));         // translate to protein string
            var result = trn.Substring(0, trn.Length - 4);                 // remove Stop, result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

        public static string DNA2RNA(string dna)
        {
            return dna.Replace('T', 'U');
        }

        public static IEnumerable<string> Split(string str, int chunkSize)
        {
            return Enumerable.Range(0, str.Length / chunkSize)
                .Select(i => str.Substring(i * chunkSize, chunkSize));
        }


        public static Dictionary<string, string> GetRNACodonTable()
        {
            var source = @"UUU F      CUU L      AUU I      GUU V
UUC F      CUC L      AUC I      GUC V
UUA L      CUA L      AUA I      GUA V
UUG L      CUG L      AUG M      GUG V
UCU S      CCU P      ACU T      GCU A
UCC S      CCC P      ACC T      GCC A
UCA S      CCA P      ACA T      GCA A
UCG S      CCG P      ACG T      GCG A
UAU Y      CAU H      AAU N      GAU D
UAC Y      CAC H      AAC N      GAC D
UAA Stop   CAA Q      AAA K      GAA E
UAG Stop   CAG Q      AAG K      GAG E
UGU C      CGU R      AGU S      GGU G
UGC C      CGC R      AGC S      GGC G
UGA Stop   CGA R      AGA R      GGA G
UGG W      CGG R      AGG R      GGG G";
            var elms = source
                .Split(new string[] { " ", Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                .ToArray();
            var chunks2 = Enumerable.Range(0, elms.Length / 2)
                .Select(idx => new string[] { elms[idx * 2], elms[idx * 2 + 1] })
                .ToArray();
            var dct = chunks2
                .ToDictionary(k => k[0], v => v[1]);
            return dct;
        }



    }
}