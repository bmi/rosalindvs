﻿using GeneticSharp.Domain;
using GeneticSharp.Domain.Chromosomes;
using GeneticSharp.Domain.Crossovers;
using GeneticSharp.Domain.Fitnesses;
using GeneticSharp.Domain.Mutations;
using GeneticSharp.Domain.Populations;
using GeneticSharp.Domain.Randomizations;
using GeneticSharp.Domain.Reinsertions;
using GeneticSharp.Domain.Selections;
using GeneticSharp.Domain.Terminations;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Policy;
using System.Threading.Tasks;

namespace RosalindVS
{
    /// <summary>
    /// Sorting by Reversals genetic algorithm - non deterministic solution
    /// </summary>
    internal class SortingByReversals
    {
        public const int PopMin = 100;
        public const int PopMax = 300;
        public const int Stagnation = 100;
        public const int MaxReversals = 10;
        public const int NoSolutionPenalty = 15;
        public const int StopAfter = 10000000;
        public const float MutationProbability = 0.5f; // original 0.1f
        public const float CrossOverMicProbability = 0.5f; // original 0.5f

        public SortingByReversals()
        {
//            var input2 = @"1 2 3 4 5 6 7 8 9 10
//1 8 9 3 2 7 6 5 4 10";
            var input = 
@"10 7 6 5 2 9 3 8 4 1
6 10 2 3 5 1 9 8 7 4";

            var inputs = input.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            DataSBR.bytes0 = inputs[0].Split(' ').Select(s => byte.Parse(s.ToString())).ToArray();
            DataSBR.bytes1 = inputs[1].Split(' ').Select(s => byte.Parse(s.ToString())).ToArray();

            Console.WriteLine($"Population: {SortingByReversals.PopMin} - {SortingByReversals.PopMax} ");
            Console.WriteLine($"CrossOverMicProbability: {SortingByReversals.CrossOverMicProbability}");
            Console.WriteLine($"MutationProbability: {SortingByReversals.MutationProbability}");

            GetSolutions();
            Console.WriteLine("Key");
            Console.ReadKey();
        }

        private void GetSolutions()
        {
            int idx = 0;
            // displaying searched solutions on Console
            var solutions = new HashSet<string>();
            while (true)
            {
                var chr = GetSolution();
                if (chr.FoundSolution)
                {
                    var sReversals = $"{chr.Reversals.Count} {string.Join(", ", chr.Reversals)}";
                    if (!solutions.Contains(sReversals)) // show new solutions only
                    {
                        solutions.Add(sReversals);
                        Console.WriteLine($"Reversals: {sReversals}");
                    }
                }
                idx++;
                if (idx >= SortingByReversals.StopAfter)
                {
                    break;
                }
            }

        }

        private SortingByReversalsChromosome GetSolution()
        {
            // GA defintion, used free GeneticSharp from NuGet
            var ichromosome = new SortingByReversalsChromosome(SortingByReversals.MaxReversals);
            var population = new Population(PopMin, PopMax, ichromosome);

            var fitness = new SortingByReversalsFitness();
            var selection = new EliteSelection();
            var crossover = new UniformCrossover();
            crossover.MixProbability = SortingByReversals.CrossOverMicProbability;
            var mutation = new TworsMutation();
            var reinsertion = new ElitistReinsertion();
            var termination = new FitnessStagnationTermination(Stagnation);

            var ga = new GeneticAlgorithm(
                population,
                fitness,
                selection,
                crossover,
                mutation);
            ga.Reinsertion = reinsertion;

            ga.Termination = termination;
            ga.MutationProbability = SortingByReversals.MutationProbability;
            ga.Start();
            return ga.BestChromosome as SortingByReversalsChromosome;
        }
    }

    /// <summary>
    /// Chromosome for swap points
    /// </summary>
    public class SortingByReversalsChromosome : ChromosomeBase
    {
        public bool FoundSolution { get; internal set; }
        public int ReversalDistance { get; internal set; }
        public List<Tuple<int, int>> Reversals { get; internal set; }

        public SortingByReversalsChromosome(int reversalCount) : base(reversalCount * 2)
        {
            for (int i = 0; i < this.Length; i++)
            {
                ReplaceGene(i, GenerateGene(i));
            }
        }

        public override IChromosome CreateNew()
        {
            return new SortingByReversalsChromosome(SortingByReversals.MaxReversals); // maxReversalsCount count;
        }

        public override Gene GenerateGene(int geneIndex)
        {
            return new Gene(RandomizationProvider.Current.GetInt(-1, 10)); // hodnoty v rozsahu -1 - 9 - indexy do pola pre body swapu
        }
    }

    /// <summary>
    /// Fitness funcntion to evaluate chromosome in Reversal Distance problem
    /// </summary>
    public class SortingByReversalsFitness : IFitness
    {
        public double Evaluate(IChromosome chromosome)
        {
            var chr = chromosome as SortingByReversalsChromosome;
            var genes = chromosome.GetGenes();
            var reversals = new List<Tuple<int, int>>();
            double fitness = 0.0;
            bool foundSolution = false;
            var data = DataSBR.Clone();
            var arr0 = data.Array0;
            var arr1 = data.Array1;
            for (int i = 0; i < chr.Length; i += 2) // genes for change points
            {
                // get start and end point of swap from genes
                var iStart = (int)genes[i].Value;
                if (iStart == -1)
                    break;
                var iEnd = (int)genes[i + 1].Value;
                if (iEnd == -1)
                    break;


                if (iStart != iEnd)
                {
                    var iMin = Math.Min(iStart, iEnd);
                    var iMax = Math.Max(iStart, iEnd);
                    chr.ReversalDistance++;
                    reversals.Add(new Tuple<int, int>(iMin + 1, iMax + 1));
                    while (iMin < iMax)
                    {
                        // swap
                        var c = arr0[iMin];
                        arr0[iMin] = arr0[iMax];
                        arr0[iMax] = c;
                        iMin++; iMax--;
                    }
                    foundSolution = Enumerable.SequenceEqual(arr0, arr1);
                    if (foundSolution)
                    {
                        chr.Reversals = reversals;
                        chr.FoundSolution = foundSolution;
                        break;
                    }
                }
            }

            // calculate fitness
            if (foundSolution)
            {
                fitness = chr.ReversalDistance;
            }
            else
            {
                double diffSum = 0;
                for (int j = 0; j < 10; j++)
                {
                    if (arr0[j] != arr1[j])
                    {
                        var posDiff = Math.Abs(j - Array.IndexOf(arr1, arr0[j]));
                        diffSum += posDiff; // suma vzdialenosot od spravnej pozicie
                    }
                }
                fitness = diffSum + chr.ReversalDistance + SortingByReversals.NoSolutionPenalty;
            }

            return 1.0 / fitness;
        }
    }

    public class DataSBR
    {
        internal static byte[] bytes0;
        internal static byte[] bytes1;


        public byte[] Array0 { get; set; }
        public byte[] Array1 { get; set; }


        internal static DataSBR Clone()
        {
            var ds = new DataSBR();
            ds.Array0 = bytes0.ToArray();
            ds.Array1 = bytes1;
            return ds;
        }
    }



}