﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Permissions;
using System.Text;

namespace RosalindVS
{
    internal class ErrorCorrectionInReads
    {
        public ErrorCorrectionInReads()
        {
            // read fasta data
            var fasta = LoadFastaInput(@"h:\Downloads\rosalind_corr_3_dataset.txt");
            var inputs = fasta.Select(i => new OneRead { StrDS = i.Value, StrREVC = GetRevComplement(i.Value) }).ToArray();
            
            // HashSet for corrections
            var hsCorrections = new HashSet<Tuple<string, string, string>>();

            // HashSet for reverse complements
            var lstRC = inputs.Select(i => i.StrREVC).ToHashSet();

            // Dictionary for dataset values
            var lstDS = inputs.GroupBy(i => i.StrDS).ToDictionary(k => k.Key, v => v.Count());

            // Collection for correct reads
            var lstCorrectReads = new List<OneRead>();

            // get correct reads
            foreach (var item in inputs)
            {
                var count = lstDS[item.StrDS];
                if (lstRC.Contains(item.StrDS))
                    count++;
                if (count > 1)
                {
                    item.IsCorrectRead = true;
                    lstCorrectReads.Add(item);
                }
            }

            var sbResult = new StringBuilder();

            // iterate incorrect reads
            foreach (var dsItem in inputs)
            {
                if (dsItem.IsCorrectRead)
                    continue;

                // find correction in correct reads
                foreach (var corrItem in lstCorrectReads)
                {
                    // test hammnig distance between source and other sources
                    if (HammingDistance(dsItem.StrDS, corrItem.StrDS) == 1)
                    {
                        // HashSet ignore duplication
                        hsCorrections.Add(new Tuple<string, string, string>(dsItem.StrDS, corrItem.StrDS, corrItem.StrREVC));
                    }
                    // test hammnig distance between source and other reverse complements
                    if (HammingDistance(dsItem.StrDS, corrItem.StrREVC) == 1)
                    {
                        // HashSet ignore duplication
                        hsCorrections.Add(new Tuple<string, string, string>(dsItem.StrDS, corrItem.StrREVC, corrItem.StrDS));
                    }
                }
            }

            // get result
            foreach (var cc in hsCorrections)
            {
                var str = $"{cc.Item1}->{cc.Item2}";
                sbResult.AppendLine(str);
            }
            var result = sbResult.ToString(); // result here
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

        public static string GetRevComplement(string src)
        {
            char[] arr = src.ToCharArray();
            Array.Reverse(arr);
            var sRevCompl = new string(arr);
            sRevCompl = sRevCompl.Replace('A', 'a').Replace('C', 'c');
            sRevCompl = sRevCompl.Replace('T', 'A').Replace('G', 'C');
            sRevCompl = sRevCompl.Replace('a', 'T').Replace('c', 'G');
            return sRevCompl;
        }

        public static int HammingDistance(string str1, string str2)
        {
            var result = 0;
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i] != str2[i])
                    result++;
            }
            return result;
        }

    }

    [DebuggerDisplay("{StrDS}, {StrREVC}, {IsCorrectRead}")]
    public class OneRead
    {
        public string StrDS { get; set; }
        public string StrREVC { get; set; }
        public bool IsCorrectRead { get; set; }

    }
}