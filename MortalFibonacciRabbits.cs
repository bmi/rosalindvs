﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace RosalindVS
{
    internal class MortalFibonacciRabbits
    {
        public MortalFibonacciRabbits(int months, int dieAfterMonths)
        {
            const ulong cntNewBorn = 1;
            var ages = new Dictionary<int, ulong>();
            for (int age = 0; age < dieAfterMonths; age++)
                ages.Add(age, 0);
            ages[0] = 1; // one young pair
            for (int month = 1; month < months; month++)
            {
                ulong cntNewBornTotal = 0;

                // breed new rabbits
                for (int age = 1; age < dieAfterMonths; age++)
                {
                    cntNewBornTotal += ages[age] * cntNewBorn;
                }

                // oldest rabbits die
                // shift rabbits to older group
                for (int age = dieAfterMonths - 1; age >= 1; age--)
                {
                    ages[age] = ages[age - 1];
                }
                ages[0] = cntNewBornTotal;
            }
            var result = ages.Sum(r => (decimal?)r.Value); // here is result

        }
    }
}