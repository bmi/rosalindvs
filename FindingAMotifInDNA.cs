﻿namespace RosalindVS
{
    internal class FindingAMotifInDNA
    {
        public FindingAMotifInDNA()
        {
            var input = "GATATATGCATATACTT";
            var substring = "ATAT";
            var result = ""; // result
            var idx = input.IndexOf(substring);
            while (idx > -1)
            {
                result += $"{idx + 1} ";
                idx = input.IndexOf(substring, idx + 1);
            }
        }
    }
}