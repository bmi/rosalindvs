﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RosalindVS
{
    internal class GAEditDistance
    {
        public GAEditDistance()
        {
            var fasta = LoadFastaInput(@"h:\Downloads\rosalind_edit.txt");
            var S = fasta.First().Value.ToCharArray();
            var T = fasta.Last().Value.ToCharArray();

            // Levenshtein distance
            // fill map
            var map = new int[S.Length + 1, T.Length + 1];
            for (int i = 0; i <= S.Length; i++)
            {
                for (int j = 0; j <= T.Length; j++)
                {
                    if (i == 0 || j == 0)
                        map[i, j] = Math.Max(i, j);
                    else
                    {
                        var m1 = map[i - 1, j] + 1;
                        var m2 = map[i, j - 1] + 1;
                        var m3 = map[i - 1, j - 1] + (S[i - 1] == T[j - 1] ? 0 : 1 );
                        map[i, j] = Math.Min(m3, Math.Min(m1, m2) );
                    }
                }
            }

            // get result
            var result = map[S.Length, T.Length];
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }
    }
}