﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RosalindVS
{
    internal class FindingAMostLikelyCommonAncestor
    {
        public FindingAMostLikelyCommonAncestor()
        {
            var input = @">Rosalind_1
ATCCAGCT
>Rosalind_2
GGGCAACT
>Rosalind_3
ATGGATCT
>Rosalind_4
AAGCAACC
>Rosalind_5
TTGGAACT
>Rosalind_6
ATGCCATT
>Rosalind_7
ATGGCACT";

            var fastaData = getFASTAData(input).ToArray();

            var columnsCount = fastaData.First().Item2.Length;
            var resultData = new Dictionary<char, int[]>();
            resultData.Add('A', new int[columnsCount]);
            resultData.Add('C', new int[columnsCount]);
            resultData.Add('G', new int[columnsCount]);
            resultData.Add('T', new int[columnsCount]);

            var sbResult = new StringBuilder();
            for (int column = 0; column < columnsCount; column++)
            {
                var groups = fastaData
                    .Select(str => str.Item2[column])
                    .GroupBy(g => g)
                    .OrderByDescending(g => g.Count());
                foreach (var grp in groups)
                    resultData[grp.Key][column] += grp.Count();
                sbResult.Append(groups.First().Key);

            }
            sbResult.AppendLine();

            var chars = new char[] { 'A', 'C', 'G', 'T' };
            foreach (var key in chars)
            {
                var keyData = resultData[key];
                var str = $"{key}: {string.Join(" ", keyData)}";
                sbResult.AppendLine(str);
            }
            
            var result = sbResult.ToString(); // here is result


            IEnumerable<Tuple<string, string>> getFASTAData(string strFASTA)
            {
                return strFASTA
                    .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                    .Select(strArr => new Tuple<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
            }
        }
    }
}