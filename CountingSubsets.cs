﻿using System;
using System.Linq;
using System.Numerics;

namespace RosalindVS
{
    internal class CountingSubsets
    {
        public CountingSubsets()
        {
            int n = 800;
            BigInteger outp = 1; // for 0 combination

            // sum combinations count of all sizes
            for (int r = 1; r <= n; r++)
                outp += nCr(n, r);

            var result = outp % 1000000; // result is here

            // alternative solution based on ON / OFF hint
            BigInteger bi = BigInteger.Pow(2, n);
            var result2 = bi % 1000000; // result is here
        }

        public static BigInteger nCr(int n, int r)
        {
            return Factorial(n) / (Factorial(r) * Factorial(n - r));
        }

        public static BigInteger Factorial(int i)
        {
            if (i <= 1)
                return 1;
            return i * Factorial(i - 1);
        }
    }
}