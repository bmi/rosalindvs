﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;

namespace RosalindVS
{
    internal class MaximumMatchingsAndRNASecondaryStructures
    {
        public MaximumMatchingsAndRNASecondaryStructures()
        {
            var input = LoadFastaInput(@"rosalind_mmch.txt").First().Value;
            var cntA = Occurences(input, "A");
            var cntU = Occurences(input, "U");
            var cntC = Occurences(input, "C");
            var cntG = Occurences(input, "G");
            var AU = Factorial(Math.Max(cntA, cntU)) / Factorial(Math.Max(cntA, cntU) - Math.Min(cntA, cntU));
            var GC = Factorial(Math.Max(cntG, cntC)) / Factorial(Math.Max(cntG, cntC) - Math.Min(cntG, cntC));

            var result = (AU * GC).ToString(); // result here
        }

        public BigInteger Factorial(BigInteger f)
        {
            if (f == 0)
                return 1;
            else
                return f * Factorial(f - 1);
        }

        public static IEnumerable<KeyValuePair<string, string>> LoadFastaInput(string filename)
        {
            return File.ReadAllText(filename)
                 .Split(new string[] { ">" }, StringSplitOptions.RemoveEmptyEntries)
                 .Select(str => str.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries))
                 .Select(strArr => new KeyValuePair<string, string>(strArr[0], string.Join("", strArr.Skip(1))));
        }

        public static int Occurences(string source, string substring)
        {
            int count = 0, n = 0;

            if (substring != "")
            {
                while ((n = source.IndexOf(substring, n, StringComparison.InvariantCulture)) != -1)
                {
                    n += substring.Length;
                    ++count;
                }
            }
            return count;
        }

    }
}